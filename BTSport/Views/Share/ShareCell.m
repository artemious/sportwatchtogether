//
//  ShareCell.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "ShareCell.h"

@interface ShareCell ()

@property (weak, nonatomic) IBOutlet UIImageView *shareOptionImageView;
@property (weak, nonatomic) IBOutlet UILabel *shareOptionTitleLabel;

@end

@implementation ShareCell

- (void)configureWithOption:(ShareOptions)option {
    
    switch (option) {
        case SMS:
        {
            self.shareOptionImageView.image = [UIImage imageNamed:@"ic_share_SMS"];
            self.shareOptionTitleLabel.text = @"SMS";
            break;
        }
        case History:
        {
            self.shareOptionImageView.image = [UIImage imageNamed:@"ic_share_history"];
            self.shareOptionTitleLabel.text = @"FRIENDS";
            break;
        }
        default:
            break;
    }
}

@end
