//
//  ShareCell.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    History,
    SMS
} ShareOptions;

static NSUInteger kNUmberOfShareOptions = 4;

@interface ShareCell : UICollectionViewCell

- (void)configureWithOption:(ShareOptions)option;

@end
