//
//  ShareView.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ShareCell.h"

@protocol ShareViewDelegate <NSObject>

- (void)didSelectShareOption:(ShareOptions)option;
- (void)shouldDismissShareView;

@end

@interface ShareView : UIView

@property (nonatomic, weak) id<ShareViewDelegate> delegate;

@end
