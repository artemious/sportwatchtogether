//
//  ShareView.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "ShareView.h"

static NSUInteger kNumberOfItemsInRow = 4;
static CGFloat kItemsOffset = 1.;
static CGFloat kCellHeight = 115.;

@interface ShareView () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *shareCollectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shareCollectionViewHeighConstraint;

@end

@implementation ShareView

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.shareCollectionView.dataSource = self;
    self.shareCollectionView.delegate = self;
    [self.shareCollectionView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    [self.shareCollectionView reloadData];
}

- (void)dealloc {
    
    [self.shareCollectionView removeObserver:self forKeyPath:@"contentSize"];
}

- (IBAction)cancelAction:(id)sender {
    
    if (self.delegate) {
        
        [self.delegate shouldDismissShareView];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return kNUmberOfShareOptions;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ShareCell *shareCell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ShareCell class]) forIndexPath:indexPath];
    
    [shareCell configureWithOption:indexPath.row];
    
    return shareCell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.delegate) {
        
        [self.delegate didSelectShareOption:indexPath.row];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake((collectionView.frame.size.width - (kItemsOffset * kNumberOfItemsInRow))/kNumberOfItemsInRow, kCellHeight);
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    self.shareCollectionViewHeighConstraint.constant = self.shareCollectionView.contentSize.height;
    
    [UIView animateWithDuration:0.25 animations:^{
        
        [self layoutIfNeeded];
    }];
}

@end
