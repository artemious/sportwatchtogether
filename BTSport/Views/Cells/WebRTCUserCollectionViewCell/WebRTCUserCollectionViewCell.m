//
//  WebRTCUserCollectionViewCell.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "WebRTCUserCollectionViewCell.h"

@interface WebRTCUserCollectionViewCell ()

@property (nonatomic) BOOL audioMutted;

@end

@implementation WebRTCUserCollectionViewCell

- (IBAction)muteAction:(id)sender {
    
    [self toogleAudio];
}

- (void)displayRenderView:(UIView*)renderView {
    
    renderView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.contentView addSubview:renderView];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:renderView attribute:NSLayoutAttributeTop multiplier:1. constant:0.]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:renderView attribute:NSLayoutAttributeBottom multiplier:1. constant:0.]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:renderView attribute:NSLayoutAttributeLeading multiplier:1. constant:0.]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:renderView attribute:NSLayoutAttributeTrailing multiplier:1. constant:0.]];
    
    [self layoutIfNeeded];
    
    self.contentView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
}

- (void)displayRenderViewForGuest:(UIView*)renderView{
    
    renderView.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.audioMutted = NO;
    
    [self.contentView addSubview:renderView];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:renderView attribute:NSLayoutAttributeTop multiplier:1. constant:0.]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:renderView attribute:NSLayoutAttributeBottom multiplier:1. constant:0.]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:renderView attribute:NSLayoutAttributeLeading multiplier:1. constant:0.]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:renderView attribute:NSLayoutAttributeTrailing multiplier:1. constant:0.]];
    
    [self layoutIfNeeded];
    
    self.contentView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    
    [self.contentView bringSubviewToFront:self.muteButton];
}

#pragma mark - Actions

- (void)toogleAudio {
    
    if (self.delegate) {
        self.audioMutted = !self.audioMutted;
        
        [self.delegate webRTCUserCell:self enableAudio:self.audioMutted];
    }
}
@end
