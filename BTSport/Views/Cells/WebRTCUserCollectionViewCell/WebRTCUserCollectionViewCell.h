//
//  WebRTCUserCollectionViewCell.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <KurentoToolbox.h>

@class WebRTCUserCollectionViewCell;

@protocol WebRTCUserCollectionViewCellDelegate <NSObject>

- (void)webRTCUserCell:(WebRTCUserCollectionViewCell*)cell enableAudio:(BOOL)enable;

@end


@interface WebRTCUserCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *muteButton;

@property (weak, nonatomic) id<WebRTCUserCollectionViewCellDelegate> delegate;

- (IBAction)muteAction:(id)sender;

- (void)displayRenderView:(UIView*)renderView;
- (void)displayRenderViewForGuest:(UIView*)renderView;

@end
