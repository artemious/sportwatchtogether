//
//  StreamInfoCell.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//

#import "StreamInfoCell.h"

@implementation StreamInfoCell

-(void)populateCell:(StreamChannel*)currentStream {
    
    UIImage* image = [UIImage imageNamed:@"logo"];
    
    self.streamImage.image = image;
    self.streamNameLabel.text = currentStream.streamTitle;
    self.descriptionLabel.text = [NSString stringWithFormat:@"%@ - %@", currentStream.streamStartTime, currentStream.streamEndTime];
}

@end
 
