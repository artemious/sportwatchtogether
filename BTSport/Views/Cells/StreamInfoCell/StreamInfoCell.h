//
//  StreamInfoCell.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "StreamChannel.h"

@interface StreamInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *streamImage;
@property (weak, nonatomic) IBOutlet UILabel *streamNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

-(void)populateCell:(StreamChannel*)currentStream;

@end
