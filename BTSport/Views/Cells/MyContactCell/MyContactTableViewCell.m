//
//  MyContactTableViewCell.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/25/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import "MyContactTableViewCell.h"
#import "MyContactsView.h"
#import "InviteFromMainViewController.h"


@interface MyContactTableViewCell()<MyContactsViewDelegate, InviteFromMainViewControllerDelegate> {
    
    Contact* cellContact;
    BOOL selectedCell;
}

@property (weak, nonatomic) IBOutlet UIImageView *contactImage;
@property (weak, nonatomic) IBOutlet UILabel *contactNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactPhoneLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkImage;

@end


@implementation MyContactTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    selectedCell = NO;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    selectedCell = !selected;
    // Configure the view for the selected state
    
    [self.checkImage setHidden:selectedCell];
}

-(void)popuplateCellWith:(Contact*)currentContact {
    
    cellContact = currentContact;
    
    self.contactImage.layer.cornerRadius = self.contactImage.frame.size.width / 2;
    self.contactImage.clipsToBounds = YES;
    
    if (currentContact.image) {
        self.contactImage.image = [UIImage imageWithData:currentContact.image];
    } else {
        self.contactImage.image = [UIImage imageNamed:@"ic_img_getContacts"];
    }

    self.contactNameLabel.text = currentContact.name;
    self.contactPhoneLabel.text = currentContact.number;
}

-(NSString*)getSelectedFriendNumberHash {
    
    if (!self.checkImage.isHidden) {
        
        return cellContact.numberHash;
    } else {
        
        return nil;
    }
}

-(Contact*)getSelectedContact {
    
    if (!self.checkImage.isHidden) {
        
        return cellContact;
    }else {
        
        return nil;
    }
}

-(NSString*)getPushId {
    
    if (!self.checkImage.isHidden) {
        
         return cellContact.pushId;
    }else {
        
        return nil;
    }
   
}

@end
