//
//  MyContactTableViewCell.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/25/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"

NS_ASSUME_NONNULL_BEGIN

@interface MyContactTableViewCell : UITableViewCell

-(void)popuplateCellWith:(Contact*)currentContact;
-(NSString*)getPushId;

@end

NS_ASSUME_NONNULL_END
