//
//  BroadcastingControlsView.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol BroadcastingControlsViewDelegate <NSObject>

- (void)shouldToggleAudio;
- (void)shouldToggleVideo;

@end

@interface BroadcastingControlsView : UIView

@property (weak, nonatomic) IBOutlet UIButton *audioButton;
@property (weak, nonatomic) IBOutlet UIButton *videoButton;

@property (weak, nonatomic) id<BroadcastingControlsViewDelegate> delegate;

@end
