//
//  BroadcastingControlsView.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "BroadcastingControlsView.h"

#import "WatchTogetherManager.h"

@interface BroadcastingControlsView ()

@property (weak, nonatomic) IBOutlet UIButton* shareButton;

@end

@implementation BroadcastingControlsView

- (IBAction)toggleAudioAction:(id)sender {
    
    //    [[WatchTogetherManager shared] enableAudio:![WatchTogetherManager shared].isAudioEnabled];
    if (self.delegate) {
        
        [self.delegate shouldToggleAudio];
    }
}

- (IBAction)videoAction:(id)sender {
    
    //    [[WatchTogetherManager shared] enableVideo:![WatchTogetherManager shared].isVideoEnabled];
    
    if (self.delegate) {
        
        [self.delegate shouldToggleVideo];
    }
}

- (IBAction)shareAction:(id)sender {
    
    //    [[WatchTogetherManager shared] stopWatching:nil];
    
    //    if (self.delegate) {
    //
    //        [self.delegate shouldDisconnect];
    //    }
}

@end
