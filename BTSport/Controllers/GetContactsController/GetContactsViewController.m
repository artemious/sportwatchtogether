//
//  GetContactsViewController.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/24/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import "GetContactsViewController.h"
#import <Contacts/Contacts.h>
#import <CommonCrypto/CommonDigest.h>
#import "Constants.h"
#import "ContactsViewController.h"
#import "Contact.h"
#import "Utils.h"
#import "UserManager.h"
#import "ContactsManager.h"
#import <MBProgressHUD.h>

@interface GetContactsViewController (){
    
    NSMutableArray* contactsArray;
}

@property (weak, nonatomic) IBOutlet UIView *viewPresented;
@property (weak, nonatomic) IBOutlet UIView *presentedChildView;
@property (weak, nonatomic) IBOutlet UIButton *allowButton;

@end

@implementation GetContactsViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    contactsArray = [NSMutableArray new];
    
    self.allowButton.layer.cornerRadius = 25;
}

#pragma mark - Actions

- (IBAction)getContacts:(id)sender {
    
    [self contactsFromAddressBook];
}

#pragma mark - Logic

-(void)contactsFromAddressBook{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [ContactsManager getContacts:^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            [self performSegueWithIdentifier:GetContactsToCantactsScreen sender:nil];
        });
    }];
}

@end
