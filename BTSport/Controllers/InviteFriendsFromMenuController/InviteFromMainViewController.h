//
//  InviteFromMainViewController.h
//  Watch Together
//
//  Created by Артём Гуральник on 10/1/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"

NS_ASSUME_NONNULL_BEGIN

@protocol InviteFromMainViewControllerDelegate <NSObject>

-(Contact*)getSelectedContact;

@end

@interface InviteFromMainViewController : UIViewController

@property (weak, nonatomic) id<InviteFromMainViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
