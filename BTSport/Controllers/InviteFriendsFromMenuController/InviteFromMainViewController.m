//
//  InviteFromMainViewController.m
//  Watch Together
//
//  Created by Артём Гуральник on 10/1/18.
//  Copyright © 2018  Артём Гуральник. All rights reserved.
//

#import "InviteFromMainViewController.h"
#import "MyContactTableViewCell.h"
#import "ApiManager.h"
#import "UserManager.h"
#import <MessageUI/MessageUI.h>
#import "ContactsManager.h"
#import "Branch/Branch.h"



@interface InviteFromMainViewController ()<UITableViewDelegate, UITableViewDataSource, MFMessageComposeViewControllerDelegate, UITextFieldDelegate> {
    
    NSMutableArray* delegates;
    NSMutableArray* namesArray;
    
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelSelectionButton;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;

@end

@implementation InviteFromMainViewController

-(NSMutableArray *) contactsForInvite {
    
    if ([self.searchTextField.text isEqualToString:@""]) {
        
        NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[UserManager shared].currentUser.userPhoneContacts];
        [array removeObjectsInArray:[UserManager shared].currentUser.userSereverContacts];
        return array;
    } else {
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", self.searchTextField.text];
        NSArray * contacts = [[UserManager shared].currentUser.userPhoneContacts filteredArrayUsingPredicate: bPredicate];
        NSMutableArray * array = [[NSMutableArray alloc] initWithArray:contacts];
        [array removeObjectsInArray:[UserManager shared].currentUser.userSereverContacts];
        return array;
    }
}

#pragma mark - View Life Cycle

- (void)viewDidLoad {

   [super viewDidLoad];
    
    self.searchTextField.delegate =self;
    
    self.titleLabel.text = @"";
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveEnterForeground:)
                                                 name:@"AppDidEnterForeground"
                                               object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
/// Uncomment to add image to the left side of seaechTextField
//    self.searchTextField.leftViewMode = UITextFieldViewModeAlways;
//    self.searchTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"/*Enter your image name*/"]];
    
    UINib* ContactCell = [UINib nibWithNibName:NSStringFromClass([MyContactTableViewCell class])
                                        bundle:nil];
    
    [self.tableView registerNib:ContactCell forCellReuseIdentifier:@"userContact"];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.inviteButton setHidden:![UserManager shared].currentUser.inviteEnabled];
    
    [ContactsManager getContacts:^{
        [self updateContactList];
    }];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)dismissKeyboard {
    [self.searchTextField resignFirstResponder];
}

- (void) receiveEnterForeground:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"AppDidEnterForeground"]) {
        NSLog (@"Successfully received the enter foreground notification!");
        [self updateContactList];
    }
}

-(void) updateContactList {
    
    delegates = [NSMutableArray new];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

-(void)get:(inout NSMutableArray*) numbers and:(inout NSMutableArray *)hashes {
    
    namesArray = [NSMutableArray new];
    
    for(NSUInteger i=0;i<delegates.count;i++) {
        
        self.delegate = delegates[i];
        
        if ([self.delegate getSelectedContact]) {
            
            for (Contact* phoneBookContact in self.contactsForInvite) {
                
                if ([phoneBookContact.number isEqualToString:[self.delegate getSelectedContact].number] ) {
                    
                    Contact* selectedContact = [self.delegate getSelectedContact];
                    
                    [numbers addObject:selectedContact.number];
                    [hashes addObject:selectedContact.numberHash];
                    [namesArray addObject:selectedContact.name];
                }
            }
        }
    }
}

-(void)setupTopBarBy:(NSUInteger) elementCount {

    if (elementCount > 0) {
        
        self.titleLabel.text = [NSString stringWithFormat:@"Selected: %lu", (unsigned long)[self.tableView indexPathsForSelectedRows].count];
        [self.searchTextField setHidden:YES];
        [self.cancelSelectionButton setEnabled:YES];
    } else {
        [self.searchTextField setHidden:NO];
        [self.cancelSelectionButton setEnabled:NO];
        self.titleLabel.text = @"";
    }
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyContactTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"userContact" forIndexPath:indexPath];
    
    self.delegate = cell;
    
    if (![delegates containsObject:cell]) {
        [delegates addObject:self.delegate];
    }
    
    Contact* currentContact = self.contactsForInvite[indexPath.row];
    
    [cell popuplateCellWith:currentContact];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSMutableArray* numbersArray = [NSMutableArray new];
    NSMutableArray* hashesArray = [NSMutableArray new];
    
    [self get:numbersArray and:hashesArray];
    [self setupTopBarBy:numbersArray.count];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableArray* numbersArray = [NSMutableArray new];
    NSMutableArray* hashesArray = [NSMutableArray new];
    
    [self get:numbersArray and:hashesArray];
    [self setupTopBarBy:numbersArray.count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contactsForInvite.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.tableView.bounds.size.height / 6;
}

#pragma mark - Actions and Logic

- (IBAction)inviteAction:(id)sender {
    
    NSMutableArray* numbersArray = [NSMutableArray new];
    NSMutableArray* hashesArray = [NSMutableArray new];
   
    [self get:numbersArray and:hashesArray];
    
    if (numbersArray.count > 0) {
        
        if([MFMessageComposeViewController canSendText]) {
            MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
            
            messageController.messageComposeDelegate = self;
            messageController.recipients = numbersArray;
            BranchUniversalObject *buo = [[BranchUniversalObject alloc] initWithCanonicalIdentifier:@"content/12345"];
            BranchLinkProperties *lp = [[BranchLinkProperties alloc] init];
            
            [buo getShortUrlWithLinkProperties:lp andCallback:^(NSString* url, NSError* error) {
                if (!error) {
                    
                    NSMutableString * message = [[NSMutableString alloc] initWithString:@"Hey"];
                    
                    for (NSString *name in namesArray) {
                        [message appendString:[NSString stringWithFormat:@", %@", name]];
                    }
                    
                    [message appendString:@", download the Watch Together Sport app so we can watch together:\n"];
                    [message appendString:url];
                    messageController.body = message;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self presentViewController:messageController animated:YES completion:NULL];
                    });
                }
            }];
        }

        [[ApiManager shared] inviteFriends:hashesArray completionBlock:^(id responce, NSError *error) {

            for (NSString* contact in hashesArray) {
                Contact* deleteContact = nil;
                for (Contact* phoneContact in self.contactsForInvite) {
                    if ([contact isEqualToString:phoneContact.numberHash]) {
                        deleteContact = phoneContact;
                    }
                }
            }
        }];
        
    } else {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please select you contacts to invite" preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction
                          actionWithTitle:@"Ok"
                          style:UIAlertActionStyleDefault
                          handler:nil]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    [self setupTopBarBy:0];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [self.tableView reloadData];
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)deselectSelectedRowsAction:(id)sender {
    
    NSArray* selectedRows = [NSArray arrayWithArray: [self.tableView indexPathsForSelectedRows]];
    
    if (selectedRows.count > 0) {
        for (NSIndexPath* indexpath in selectedRows) {
            [self.tableView deselectRowAtIndexPath:indexpath animated:YES];
        }
        
        [self setupTopBarBy:0];
    }
}

- (IBAction)searchTextFieldTextChanged:(UITextField *)sender {
    [self.tableView reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField endEditing:YES];
    
    return YES;
}

@end
