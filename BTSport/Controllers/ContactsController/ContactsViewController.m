//
//  ContactsViewController.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/24/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import <MBProgressHUD.h>
#import "ContactsViewController.h"
#import "IBRPagerTabStrip.h"
#import "MyContactsView.h"
#import "Constants.h"
#import "ApiManager.h"
#import "Contact.h"
#import "Utils.h"
#import "UserManager.h"
#import "ContactsManager.h"

@interface ContactsViewController ()<IBRPagerTabStripDelegate, SendingMessages> {
    
    NSMutableArray* serverFriendsArray;
}

@property (weak, nonatomic) IBOutlet IBRPagerTabStrip *pageStripView;
@property (nonatomic) MyContactsView *phoneFirendsView;
@property (nonatomic) MyContactsView *serverFriendsView;

@end

@implementation ContactsViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [ContactsManager getFriends:self and:^{
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self setupUI];
    }];
}

-(void)setupUI {
    
    self.phoneFirendsView = [[MyContactsView alloc] initWithFrame:[self.pageStripView boundsForView]];
    [self.phoneFirendsView customInit:kPhoneContactsType];
    self.phoneFirendsView.messagesDelegate = self;
    
    self.serverFriendsView = [[MyContactsView alloc] initWithFrame:[self.pageStripView boundsForView]];
    [self.serverFriendsView customInit:kServerContactsType];
    
    self.pageStripView.heightTab = 120;
    self.pageStripView.colorBtnTabTextActive = [UIColor colorWithRed:0.7 green:0.33 blue:0.93 alpha:1];
    self.pageStripView.colorBtnTabTextInactive = [UIColor colorWithRed:0.35 green:0.35 blue:0.35 alpha:1];
    self.pageStripView.colorViewRunner = [UIColor colorWithRed:0.7 green:0.33 blue:0.93 alpha:1];
    self.pageStripView.heightViewRunner = 2;
    
    self.pageStripView.delegate = self;
    
    [self.pageStripView addTabs:@[@{@"title": @"FRIENDS ON SPORT WATCH TOGETHER", @"view": self.serverFriendsView},
                                  @{@"title": @"INVITE YOUR CONTACTS", @"view":  self.phoneFirendsView}]];
}

#pragma mark - IBRPagerTabStripDelegate

- (void)pageChangedToPage:(int)page {
    
    switch (page) {
        case 0:
            
            break;
        case 1:
            
            break;
        default:
            break;
    }
}

-(void)sendMessage:(UIViewController*)messageController  {
    
    [self presentViewController:messageController animated:YES completion:NULL];
}

@end
