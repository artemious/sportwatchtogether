//
//  MyContactsView.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/24/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import <MBProgressHUD.h>
#import "MyContactsView.h"
#import "MyContactTableViewCell.h"
#import "Contact.h"
#import "ApiManager.h"
#import "Utils.h"
#import "ContactsViewController.h"
#import "Constants.h"
#import "UserManager.h"
#import "ContactsManager.h"
#import <MessageUI/MessageUI.h>

@interface MyContactsView()<UITableViewDelegate, UITableViewDataSource, MFMessageComposeViewControllerDelegate, UITextFieldDelegate> {
    
    NSMutableArray* delegates;
    ContactsViewType currentType;
    NSMutableArray* invitedHashes;
    NSMutableArray* invitedContacts;
}

@property (nonatomic) UIRefreshControl* refreshControl;
@property (strong, nonatomic) IBOutlet UIView *customView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;

@end


@implementation MyContactsView

-(NSMutableArray *) contactsData {
    
    switch (currentType) {
        case kPhoneContactsType:
            if ([self.searchTextField.text isEqualToString:@""]) {
                
                NSMutableArray *array = [[NSMutableArray alloc] initWithArray:[UserManager shared].currentUser.userPhoneContacts];
                [array removeObjectsInArray:[UserManager shared].currentUser.userSereverContacts];
                [array removeObjectsInArray:invitedContacts];
                return array;
            } else {
                NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", self.searchTextField.text];
                NSArray * contacts = [[UserManager shared].currentUser.userPhoneContacts filteredArrayUsingPredicate: bPredicate];
                NSMutableArray * array = [[NSMutableArray alloc] initWithArray:contacts];
                [array removeObjectsInArray:[UserManager shared].currentUser.userSereverContacts];
                [array removeObjectsInArray:invitedContacts];
                return array;
            }
            
        case kServerContactsType:
            if ([self.searchTextField.text isEqualToString:@""]) {
                return [UserManager shared].currentUser.userSereverContacts;
            } else {
                NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", self.searchTextField.text];
                NSArray * contacts = [[UserManager shared].currentUser.userSereverContacts filteredArrayUsingPredicate: bPredicate];
                NSMutableArray * array = [[NSMutableArray alloc] initWithArray:contacts];
                return array;
            }
    }
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) customInit:(ContactsViewType)viewType {
    
    self.searchTextField.delegate = self;
    
    invitedHashes = [NSMutableArray new];
    delegates = [NSMutableArray new];
    
    [[NSBundle mainBundle] loadNibNamed:@"MyContactsView" owner:self options:nil];
    
    self.customView.frame = self.bounds;
    
    UINib* ContactCell = [UINib nibWithNibName:NSStringFromClass([MyContactTableViewCell class])
                                               bundle:nil];
    
    [self.tableView registerNib:ContactCell forCellReuseIdentifier:@"userContact"];
    
    currentType = viewType;
    
    self.inviteButton.layer.cornerRadius = 25;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self addGestureRecognizer:tap];
    
    
    switch (viewType) {
            
        case kServerContactsType:
            
            [self.inviteButton  setTitle:@"LET`S START" forState:UIControlStateNormal];
            
            self.refreshControl = [[UIRefreshControl alloc] init];
            
            [self.refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
            
            self.tableView.allowsSelection = NO;
            
            [self.tableView addSubview:self.refreshControl];
            
            break;
            
        case kPhoneContactsType:
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(receiveEnterForeground:)
                                                         name:@"AppDidEnterForeground"
                                                       object:nil];
            
            [self.inviteButton setHidden:![UserManager shared].currentUser.inviteEnabled];
            [self.inviteButton  setTitle:@"INVITE TO FRIENDS" forState:UIControlStateNormal];
            
            break;
        default:
            break;
    }
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.tableView reloadData];
    
    [self addSubview:self.customView];
}

- (void) receiveEnterForeground:(NSNotification *) notification {
    
    if (currentType == kPhoneContactsType) {
        if ([[notification name] isEqualToString:@"AppDidEnterForeground"]) {
            NSLog (@"Successfully received the enter foreground notification!");
            [self updateContactList];
        }
    }
}

-(void) updateContactList {
    
    delegates = [NSMutableArray new];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

-(void)dismissKeyboard {
    [self.searchTextField resignFirstResponder];
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyContactTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"userContact" forIndexPath:indexPath];
    
    self.delegate = cell;
    
    [delegates addObject:self.delegate];
    

    if (self.contactsData.count) {
        
        Contact* currentContact = self.contactsData[indexPath.row];
        
        [cell popuplateCellWith:currentContact];
    }
    
    
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.contactsData.count == 0 && currentType == kServerContactsType) {
        
        [self.inviteButton setEnabled:YES];
    } else {
        
        [self.inviteButton setEnabled:YES];
    }
    
    return self.contactsData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return self.tableView.bounds.size.height / 6;
}

#pragma mark -Logic

-(void) refreshData {

    [self.refreshControl beginRefreshing];

    [MBProgressHUD showHUDAddedTo:self.tableView animated:YES];

    [ContactsManager getFriends:self and:^{
        [MBProgressHUD hideHUDForView:self.tableView animated:YES];
        [self.tableView reloadData];
    }];
    
    if (self.contactsData.count > 0) {

        [self.inviteButton setEnabled:YES];
    }

    [self.refreshControl endRefreshing];
}


#pragma mark - Actions

- (IBAction)inviteOrCOntinueButton:(id)sender {
    
    switch (currentType) {
            
        case kServerContactsType:
            
            if ([self.nextResponder.nextResponder.nextResponder.nextResponder isKindOfClass: [ContactsViewController class]]) {
                
                ContactsViewController* parent = (ContactsViewController*)self.nextResponder.nextResponder.nextResponder.nextResponder;
                
                [parent performSegueWithIdentifier:ContactsToMainScreen sender:nil];
            }
            
            break;
        case kPhoneContactsType:
            
            [invitedHashes removeAllObjects];
            NSMutableArray* numbersArray = [NSMutableArray new];
            NSMutableArray* namesArray = [NSMutableArray new];
            
            for(NSUInteger i=0;i<delegates.count;i++) {
                
                self.delegate = delegates[i];
                
                if ([self.delegate getSelectedFriendNumberHash]) {
                    for (Contact* phoneBookContact in self.contactsData) {
                        if ([phoneBookContact.number isEqualToString:[self.delegate getSelectedContact].number]) {
                            
                            Contact* selectedContact = [self.delegate getSelectedContact];
                            
                            [numbersArray addObject:selectedContact.number];
                            [namesArray addObject:selectedContact.name];
                            [invitedHashes addObject: [self.delegate getSelectedFriendNumberHash]];
                        }
                    }
                }
            }

            if (invitedHashes.count == 0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:@"Please select your friends to invite"
                                                               delegate:self
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles:nil];

                [alert show];
                
            } else {
                
                if([MFMessageComposeViewController canSendText]) {
                    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
                    
                    messageController.messageComposeDelegate = self;
                    messageController.recipients = numbersArray;
                    BranchUniversalObject *buo = [[BranchUniversalObject alloc] initWithCanonicalIdentifier:@"content/12345"];
                    BranchLinkProperties *lp = [[BranchLinkProperties alloc] init];
                    
                    [buo getShortUrlWithLinkProperties:lp andCallback:^(NSString* url, NSError* error) {
                        if (!error) {
                            
                            NSMutableString * message = [[NSMutableString alloc] initWithString:@"Hey"];
                            
                            for (NSString *name in namesArray) {
                                [message appendString:[NSString stringWithFormat:@", %@", name]];
                            }
                            
                            [message appendString:@", download the Watch Together Sport app so we can watch together:\n"];
                            [message appendString:url];
                            messageController.body = message;
                            dispatch_async(dispatch_get_main_queue(), ^{
                            
                                [self.messagesDelegate sendMessage:messageController];
                            });
                        }
                    }];
                }
            }
            [self.tableView reloadData];
            break;
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    if (result == MessageComposeResultSent) {
        [MBProgressHUD showHUDAddedTo:self animated:YES];
        [[ApiManager shared] inviteFriends:invitedHashes completionBlock:^(id responce, NSError *error) {
            [MBProgressHUD hideHUDForView:self animated:YES];
            
            for (NSString* contact in invitedHashes) {
                
                Contact* deleteContact = nil;
                for (Contact* phoneContact in self.contactsData) {
                    
                    if ([contact isEqualToString:phoneContact.numberHash]) {
                        
                        deleteContact = phoneContact;
                    }
                }
                
                [invitedContacts addObject:deleteContact];
            }
        }];
    }
    
    [self updateContactList];
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)searchTextFieldTextChanged:(id)sender {
    [self.tableView reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField endEditing:YES];
    
    return YES;
}

@end
