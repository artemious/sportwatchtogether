//
//  MyContactsView.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/24/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "ContactsManager.h"
#import "Contact.h"

NS_ASSUME_NONNULL_BEGIN

@protocol MyContactsViewDelegate <NSObject>

-(NSString*)getSelectedFriendNumberHash;
-(Contact*)getSelectedContact;

@end

@interface MyContactsView : UIView

@property (weak, nonatomic) id<MyContactsViewDelegate> delegate;
@property (weak, nonatomic) id<SendingMessages> messagesDelegate;

-(void) customInit:(ContactsViewType)viewType;

@end

NS_ASSUME_NONNULL_END
