//
//  StreamDetailsController.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "StreamDetailsController.h"
#import "InvitesManager.h"
#import "ApiManager.h"

@interface StreamDetailsController ()

@end

@implementation StreamDetailsController

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

# pragma mark - Check for invitation

- (void)handleNotificationIfNeeded {
    
    __block WatchInvite *watchInvite = [[InvitesManager shared] getUnhandledWatchInvite];
    
    if (watchInvite && !self.presentedViewController) {
        
        NSString *message = [watchInvite getInvitationMessage];
        
        UIAlertController *invitationAlert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
        
        [invitationAlert addAction:[UIAlertAction actionWithTitle:@"Join" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self acceptInvitation];
            
        }]];
        
        [invitationAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [[ApiManager shared] logOpenLinkAction:watchInvite.getBranchLink completionBlock:nil];
            
            watchInvite.isHandled = YES;
            
        }]];
        
        [self presentViewController:invitationAlert animated:YES completion:nil];
    }
}

- (void)acceptInvitation {
    
    //[self performSegueWithIdentifier:StreamsListToStreamDetails sender:nil];
}

@end
