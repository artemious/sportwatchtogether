//
//  StreamsListController.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "StreamsListController.h"
#import "StreamWatchController.h"
#import "StreamDetailsController.h"
#import "StreamInfoCell.h"
#import "ApiManager.h"
#import "Constants.h"
#import "StreamChannel.h"
#import "Utils.h"
#import <MBProgressHUD.h>
#import "InvitesManager.h"
#import "UserManager.h"

@interface StreamsListController ()<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) StreamChannel* currentStream;
@property (strong, nonatomic) NSArray* chanelInfo;

@property (nonatomic) UIRefreshControl* refreshControl;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *exceptionLabel;

@end

@implementation StreamsListController

static NSString * const reuseIdentifier = @"channelCell";

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];

    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    self.channelUrl = @"https://sportwt.sceenic.co/api/link/get_all";

    self.currentStream = [[StreamChannel alloc] init];
    self.navigationItem.hidesBackButton = NO;
    
    [self.tableView reloadData];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    
    [self.refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshControl];
    [self refreshData];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self configureNavigation];
    [self handleNotificationIfNeeded ];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.chanelInfo.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    StreamInfoCell* streamCell = [tableView dequeueReusableCellWithIdentifier: reuseIdentifier];
    
    self.currentStream = [[StreamChannel alloc] initWithData:self.chanelInfo[indexPath.row]];
        [streamCell populateCell:self.currentStream];
        
        streamCell.contentView.layer.borderColor = [UIColor colorWithRed:11/255.0 green:49/255.0 blue:70/255.0 alpha:0.15].CGColor;
        [streamCell.contentView.layer setBorderWidth:1.0f];
    
    return streamCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.currentStream = [[StreamChannel alloc] initWithData:self.chanelInfo[indexPath.row]];
    
    [self performSegueWithIdentifier:StreamsListToStreamWatch sender:self];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Actions

- (IBAction)refreshAction:(id)sender {
    
    [self loadData];
}


#pragma mark - Navigation configurator

-(void)configureNavigation {
    
    self.navigationController.navigationBar.backgroundColor = [UIColor darkTextColor];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor darkTextColor]}];
    
    UIImage* image = [UIImage imageNamed:@"logoNavBar"];
    CGRect frame = CGRectMake(0, 0, image.size.width, image.size.height);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frame];
    [someButton setBackgroundImage:image forState:UIControlStateNormal];
    someButton.userInteractionEnabled = NO;

    UIBarButtonItem *logoButton = [[UIBarButtonItem alloc] initWithCustomView:someButton];

    self.navigationItem.leftBarButtonItem = logoButton;
}

- (void)backButtonPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

# pragma mark - Check for invitation

- (void)handleNotificationIfNeeded {
    
    __block WatchInvite *watchInvite = [[InvitesManager shared] getUnhandledWatchInvite];
    
    if (watchInvite) {
        
        NSString *message = [watchInvite getInvitationMessage];
        
        UIAlertController *invitationAlert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
        
        [invitationAlert addAction:[UIAlertAction actionWithTitle:@"Join" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self acceptInvitation];
            
        }]];
        
        [invitationAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            
            watchInvite.isHandled = YES;
            
        }]];
        
        [self presentViewController:invitationAlert animated:YES completion:nil];
    }
}

- (void)acceptInvitation {
    

    [self performSegueWithIdentifier:StreamsListToStreamWatch sender:nil];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if ([segue.identifier isEqualToString: StreamsListToStreamWatch]) {
        
        if (sender == nil) {

            StreamWatchController *streamWatchController = (StreamWatchController*)segue.destinationViewController;
            
            WatchInvite *watchInvite = [[InvitesManager shared] getUnhandledWatchInvite];

            watchInvite.isHandled = YES;

            [UserManager shared].currentUser.roomToConnectTo = watchInvite.getRoomID;
            
            streamWatchController.videoStreamId = watchInvite.getVideoID;
            
            [UserManager shared].currentUser.userVideoURL = watchInvite.getVideoID;
            [UserManager shared].currentUser.userVideoName = watchInvite.getChannelName;
            [UserManager shared].currentUser.roomToConnectTo = watchInvite.getRoomID;

        } else {
            
            StreamWatchController *streamWatchController = (StreamWatchController*)segue.destinationViewController;
    
            streamWatchController.videoStreamId = self.currentStream.streamURL;
            
            [UserManager shared].currentUser.roomToConnectTo = [UserManager shared].currentUser.deviceid;
            [UserManager shared].currentUser.userVideoURL = self.currentStream.streamURL;
            [UserManager shared].currentUser.userVideoName = self.currentStream.streamTitle;
        }
    }
}

#pragma mark - Helpers

-(void) refreshData {
    
    [self.refreshControl beginRefreshing];
    
    [self loadData];
}

- (void) loadData {
#pragma mark - Helpers ====== FOR DEBUG
    NSDictionary* stream = @{@"url" : @"http://35.237.251.177/test.m3u8",
                            @"title" : @"Test",
                            @"start_time" : @"",
                            @"end_time" : @""
                             };

    NSDictionary* stream1 = @{@"url" : @"https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8",
                             @"title" : @"Test Sinte",
                             @"start_time" : @"",
                             @"end_time" : @""
                             };
    NSDictionary* stream2 = @{@"url" : @"http://184.72.239.149/vod/smil:BigBuckBunny.smil/playlist.m3u8",
                             @"title" : @"Test BigBuckBunny",
                             @"start_time" : @"",
                             @"end_time" : @""
                             };
    NSDictionary* stream3 = @{@"url" : @"https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8",
                             @"title" : @"Test Bitdash-a.akamaihd",
                             @"start_time" : @"",
                             @"end_time" : @""
                             };
    NSDictionary* stream4 = @{@"url" : @"http://www.streambox.fr/playlists/test_001/stream.m3u8",
                             @"title" : @"Test StreamBox test_001",
                             @"start_time" : @"",
                             @"end_time" : @""

                             };
    self.chanelInfo = [NSArray arrayWithObjects:stream1,stream2,stream3,stream4, nil];


    [self.refreshControl endRefreshing];
//
//    [[ApiManager shared] getStreamsList:self.channelUrl andCompletion:^(id responce, NSError *error) {
//
//        [self.refreshControl endRefreshing];
//
//        if ([responce[@"ok"]  intValue] == 0 && [responce[@"code"]  intValue] == 2) {
//
//            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentUser"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//
//            [self.navigationController popToRootViewControllerAnimated:YES];
//
//        }else {
//
//            NSDictionary* responceDictionary = responce;
//
//            self.chanelInfo = [responceDictionary objectForKey:@"data"];
//
//            if (self.chanelInfo.count ==  0) {
//
//                self.exceptionLabel.text = @"There is no Live Stream Video on the Channel now";
//
//                [self.exceptionLabel setHidden:NO];
//            } else {
//
//                [self.exceptionLabel setHidden:YES];
//                [self.tableView reloadData];
//            }
//        }
//    }];
}


@end
