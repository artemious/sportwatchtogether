//
//  NavigationController.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "NavigationController.h"
#import "StreamWatchController.h"
#import "HistoryController.h"

@interface NavigationController ()

@end

@implementation NavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [super pushViewController:viewController animated:animated];
    
    [self updateOrientaion];
}

- (nullable UIViewController *)popViewControllerAnimated:(BOOL)animated
{
    [self updateOrientaion];
    
    return [super popViewControllerAnimated:animated];
}


- (void)updateOrientaion
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIViewController attemptRotationToDeviceOrientation];
    });
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([self.topViewController isKindOfClass:[StreamWatchController class]] || [self.topViewController isKindOfClass:[HistoryController class]]) {
        
        return UIInterfaceOrientationMaskLandscapeRight;
    }
    
    return UIInterfaceOrientationMaskPortrait;
}

@end
