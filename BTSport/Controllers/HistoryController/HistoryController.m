//
//  HistoryController.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "HistoryController.h"
#import "NavigationController.h"
#import "MyContactTableViewCell.h"
#import "Utils.h"
#import "UserManager.h"
#import "ApiManager.h"
#import <MBProgressHUD.h>
#import <Firebase.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <WebRTC/RTCMediaStream.h>
#import <WebRTC/RTCDataChannel.h>
#import "WatchTogetherManager.h"


@interface HistoryController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *historyTableView;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation HistoryController

#pragma mark - View Life Cycel

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [[UserManager shared].currentUser addItemsObserver:self];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
    [UINavigationController attemptRotationToDeviceOrientation];
    
    [self initialConfigurations];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
}

#pragma mark - Configurations

- (void)initialConfigurations {
    
    self.historyTableView.tableFooterView = [UIView new];
    self.historyTableView.allowsMultipleSelection = YES;
  
    [self.infoLabel setHidden:([UserManager shared].currentUser.userSereverContacts.count > 0)];
    
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];

    
    UINib* ContactCell = [UINib nibWithNibName:NSStringFromClass([MyContactTableViewCell class])
                                        bundle:nil];
    
    [self.historyTableView registerNib:ContactCell forCellReuseIdentifier:@"userContact"];}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeRight;
}

#pragma mark - Actions

- (IBAction)backButtonPressed:(id)sender {
    
    
    [[UserManager shared].currentUser removeItemsObserver:self];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)inviteAction:(id)sender {
    
    NSMutableArray *pushIDs = [NSMutableArray new];
    
    for (NSIndexPath *selectedIndexPath in self.historyTableView.indexPathsForSelectedRows) {
        
        MyContactTableViewCell *historyCell = [self.historyTableView cellForRowAtIndexPath:selectedIndexPath];
        
        if ([historyCell getPushId]) {
            
            [pushIDs addObject:[historyCell getPushId]];
        }
        
        [self.historyTableView deselectRowAtIndexPath:selectedIndexPath animated:YES];
    }
    
    if (pushIDs.count) {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        BranchUniversalObject *branchUniversalObject = [Utils getBranchUniversalObjectWith:self.videoStreamId
                                                                                       and:self.channelName];
        
        BranchLinkProperties *linkProperties = [[BranchLinkProperties alloc] init];
        linkProperties.feature = @"sharing";
        
        [branchUniversalObject getShortUrlWithLinkProperties:linkProperties andCallback:^(NSString * _Nullable url, NSError * _Nullable error) {
            
            if (!error && url) {
                
                NSDictionary* paramsDict = @{ @"registration_ids": pushIDs,
                                              @"notification" : @{
                                                    @"body": @"Join me to watch together.",
                                                    @"title": [UserManager shared].currentUser.fullname,
                                                    },
                                              @"data" : @{
                                                      @"user_name" : [UserManager shared].currentUser.fullname,
                                                      @"room" : [UserManager shared].currentUser.currentRoom,
                                                      @"channel_name" : [UserManager shared].currentUser.userVideoName,
                                                      @"stream_url": [UserManager shared].currentUser.userVideoURL
                                                      }
                                              };
                
                
                [[ApiManager shared] sendNotificationForGuestConversation:paramsDict andCompletion:^(BOOL success) {
                    
                    if (success) {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            NSLog(@"Notification posted");
                            
                            MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
                            
                            UIImageView *copiedImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_check_large"]];
                            
                            hud.mode = MBProgressHUDModeCustomView;
                            hud.customView = copiedImageView;
                            hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
                            hud.bezelView.backgroundColor = [UIColor clearColor];
                            hud.label.text = @"Invite sent";
                            hud.label.textColor = [UIColor whiteColor];
                            hud.label.font = [UIFont fontWithName:@"Lato-Bold" size:24.];
                            hud.backgroundView.style = MBProgressHUDBackgroundStyleBlur;
                            hud.backgroundView.color = [[UIColor blackColor] colorWithAlphaComponent:0.4];
                            
                            [self performSelector:@selector(hideSuccessHudAndDismiss) withObject:self afterDelay:1];
                        });
                    }else {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            
                            NSLog(@"Error occured while posting notification");
                            
                            UIAlertController *notificationErrorAlert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Error occured while posting notification. Please try again." preferredStyle:UIAlertControllerStyleAlert];
                            
                            [notificationErrorAlert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                            
                            [self presentViewController:notificationErrorAlert animated:YES completion:nil];
                            
                        });
                    }
                }];
            } else {
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }
            
        }];
    }
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return self.historyTableView.bounds.size.height / 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.currentFriends.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyContactTableViewCell *historyCell = [tableView dequeueReusableCellWithIdentifier:@"userContact"];
    
    Contact* cellContact = self.currentFriends[indexPath.row];
    [historyCell popuplateCellWith: cellContact] ;
    
    return historyCell;
}

#pragma mark - UITableViewDelegate

- (nullable NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSIndexPath *indexPathToSelect = nil;
    
    if (tableView.indexPathsForSelectedRows.count < 4) {
        
        indexPathToSelect = indexPath;
    }
    
    return indexPathToSelect;
}

#pragma mark - Helpers

- (void)hideSuccessHudAndDismiss {
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    
    [hud hideAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    self.currentFriends = [NSMutableArray arrayWithArray:[UserManager shared].currentUser.userSereverContacts];
    
    for (Contact* currentContact in [UserManager shared].currentUser.userSereverContacts) {
        
        for (NBMPeer* currentPeer in [UserManager shared].currentUser.userPeers) {
            
            if ([currentPeer.emailNumberHash isEqualToString:currentContact.numberHash]) {
                
                [self.currentFriends removeObject:currentContact];
            }
        }
    }
    
    [self.historyTableView reloadData];
}

@end
