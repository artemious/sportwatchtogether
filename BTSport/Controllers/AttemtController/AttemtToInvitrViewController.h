//
//  AttemtToInvitrViewController.h
//  Watch Together
//
//  Created by Артём Гуральник on 10/12/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN


@protocol AttemtToInvitrViewControllerDelegate <NSObject>

-(void)showInvitePage;

@end

@interface AttemtToInvitrViewController : UIViewController

@property (weak, nonatomic) id<AttemtToInvitrViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
