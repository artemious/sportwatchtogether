//
//  AttemtToInvitrViewController.m
//  Watch Together
//
//  Created by Артём Гуральник on 10/12/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import "AttemtToInvitrViewController.h"

@interface AttemtToInvitrViewController ()

@end

@implementation AttemtToInvitrViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
    [UINavigationController attemptRotationToDeviceOrientation];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (IBAction)dismiss:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)showInvitePage:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        [self.delegate showInvitePage];
    }];
}

@end
