//
//  MainTabViewController.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/26/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainTabViewController : UITabBarController

@end

NS_ASSUME_NONNULL_END
