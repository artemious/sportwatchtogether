//
//  MainTabViewController.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/26/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import "MainTabViewController.h"
#import "ApiManager.h"
#import <Contacts/Contacts.h>
#import <CommonCrypto/CommonDigest.h>
#import "UserManager.h"
#import "Utils.h"
#import "Contact.h"
#import "ContactsManager.h"
#import <MBProgressHUD.h>

@interface MainTabViewController (){
    
    NSMutableArray* contactsArray;
}

@end

@implementation MainTabViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];

    NSLog(@"%@", [Utils connectedNetworkType]);
    
    if ([Utils isJailBroken]) {
        
        [[ApiManager shared] deviceIsJailborkenAPI];
        
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentUser"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        [UserManager shared].currentUser = nil;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [ContactsManager getContacts:^{
        
        [ContactsManager getFriends:self and:^{
            
            NSLog (@"Friends were got from server!");
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if (![User getSavedUser]) {
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }];
    }];
    
    self.selectedIndex = 1;
    
    NSInteger offset = 7;
    
    UIEdgeInsets imageInset = UIEdgeInsetsMake(offset, 0, -offset, 0);
    for (UITabBarItem *item in self.tabBar.items) {
        
        item.imageInsets = imageInset;
    }
    
}

-(NSString*)sha256HashFor:(NSString*)input {
    
    const char* str = [input UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
    {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

@end
