//
//  SplashController.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface SplashController : UIViewController

- (void)showNextScreen;

@end
