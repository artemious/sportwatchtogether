//
//  SplashController.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "SplashController.h"
#import "User.h"
#import "UserManager.h"
#import "Constants.h"
#import "ApiManager.h"
#import "InvitesManager.h"
#import <MBProgressHUD.h>
#import "Utils.h"

@interface SplashController ()

@end

@implementation SplashController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initialConfigurations];    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self checkUserAndProccedIfPossible];
}

- (void) viewDidLayoutSubviews {
    
    [self initialConfigurations];
}

#pragma mark - Configurations

- (void)initialConfigurations {
    
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];

}

#pragma mark - Helpers

- (void)checkUserAndProccedIfPossible {
    
    User *savedUser = [User getSavedUser];
    
    if (savedUser) {

        if (![Utils isJailBroken]) {
            [UserManager shared].currentUser = savedUser;
            [[ApiManager shared] updateAppVersion];
            
            [self performSegueWithIdentifier:SplashToStreamList sender:self];
        }else {
            
            [[ApiManager shared] deviceIsJailborkenAPI];
            
            [UserManager shared].currentUser = savedUser;
            [[ApiManager shared] deviceIsJailborkenAPI];
            
            [UserManager shared].currentUser = nil;
            
            [self showNextScreen];
        }
    }else {
        
        [self showNextScreen];
    }
}

- (void)showNextScreen {

        [self performSegueWithIdentifier:SplashToLoginSegue sender:self];
   
}

# pragma mark - Check for invitation

- (void)handleInitialInvite:(InitialInvite*)initialInvite {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[ApiManager shared] loginWithEmail:initialInvite.getUserEmail completionBlock:^(id responce, NSError *error) {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        if (!error) {
            
            if ([responce isKindOfClass:[NSDictionary class]]) {
                
                if ([(NSNumber*)responce[@"IsSuccess"] boolValue]) {
                    
//                    User *currentUser = [User initWithData:responce];
//                    currentUser.number = initialInvite.getUserEmail;
//                    
//                    [currentUser save];
//                    
//                    [UserManager shared].currentUser = currentUser;
//                    
                    
                } else {
                    
                    [self performSegueWithIdentifier:SplashToLoginSegue sender:self];
                }
                
            } else {
                
                [self performSegueWithIdentifier:SplashToLoginSegue sender:self];
            }
            
        } else {
            
            [self performSegueWithIdentifier:SplashToLoginSegue sender:self];
        }
        
    }];
}

@end
