//
//  LoginController.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "LoginController.h"
#import "ApiManager.h"
#import "User.h"
#import "UserManager.h"
#import "Constants.h"
#import "Utils.h"
#import "InvitesManager.h"
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <MBProgressHUD.h>
#import <SafariServices/SafariServices.h>
#import "Checkbox.h"


typedef NS_ENUM(NSUInteger, TextfieldType) {
    firstname,
    lastname,
    email
};

typedef NS_ENUM(NSUInteger, ActionType) {
    login,
    signup
};

@interface LoginController () <UITextFieldDelegate> {
    
    BOOL loginEnabled;
    NSString* number;
    CGFloat keyboardHeight;
}

@property (weak, nonatomic) IBOutlet Checkbox *acceptTermOfUse;

@property (weak, nonatomic) IBOutlet UIView *textFieldContainerView;
@property (weak, nonatomic) IBOutlet UIView *lastNameContainer;
@property (weak, nonatomic) IBOutlet UIView *firstNameCotainer;

@property (weak, nonatomic) IBOutlet UITextField *firstnameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *lastnameTextfield;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumber;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *termOfUseButton;

@end

@implementation LoginController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    loginEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"loginEnabled"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard:)];
    [self.view addGestureRecognizer:tap];
    
    if([Utils isJailBroken]) {
        
        NSError *isJailBrokenError = [[NSError alloc] initWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey :@"Apologies, our application does not run on rooted devices"}];
        
        [Utils showAlertWithError:isJailBrokenError fromViewController:self];
    }
    
    [self.navigationItem setHidesBackButton:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self configureUI];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) viewDidLayoutSubviews {
    
    [self configureUI];
}

-(void)dismissKeyboard:(UITapGestureRecognizer *)recognizer {
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

#pragma mark - Configurations

- (void)configureUI {
    
    self.navigationItem.hidesBackButton = YES;
    
    self.textFieldContainerView.layer.cornerRadius = 25;
    self.textFieldContainerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.textFieldContainerView.layer.borderWidth = 0.3;
    
    self.lastNameContainer.layer.cornerRadius = 25;
    self.lastNameContainer.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.lastNameContainer.layer.borderWidth = 0.3;
    
    self.firstNameCotainer.layer.cornerRadius = 25;
    self.firstNameCotainer.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.firstNameCotainer.layer.borderWidth = 0.3;
    
    [self.mobileNumber setKeyboardType:UIKeyboardTypeNumberPad];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.loginButton.bounds;
    gradient.colors = @[(id)[UIColor colorWithRed:0.58 green:0.31 blue:0.85 alpha:1.0].CGColor, (id)[UIColor colorWithRed:0.71 green:0.33 blue:0.94 alpha:1.0].CGColor];
    
    gradient.locations = @[@1, @0];
    
    gradient.cornerRadius = 25;
    
    NSMutableAttributedString * title = [[NSMutableAttributedString alloc] initWithString:[[NSUserDefaults standardUserDefaults] boolForKey:@"UserSecondLogin"] ? @"INVITE YOUR FRIENDS" : @"WATCH TOGETHER"];
    NSRange selectedRange = NSMakeRange(0, title.length);
    
    [title beginEditing];
    
#define IDIOM    UI_USER_INTERFACE_IDIOM()
#define IPAD     UIUserInterfaceIdiomPad
    
    if ( IDIOM == IPAD ) {
        [title addAttribute:NSFontAttributeName
                      value: [UIFont systemFontOfSize: loginEnabled ? 25.0 : 20.0]
                      range:selectedRange];
    } else {
        [title addAttribute:NSFontAttributeName
                      value: [UIFont systemFontOfSize: loginEnabled ? 15.0 : 12.0]
                      range:selectedRange];
    }

    [title addAttribute:NSForegroundColorAttributeName
                  value:[UIColor whiteColor]
                  range:selectedRange];
    
    [title endEditing];
    
    [self.loginButton setAttributedTitle:title forState:UIControlStateNormal];
    [self.loginButton.layer insertSublayer:gradient atIndex:0];
    self.loginButton.titleLabel.numberOfLines = 0;
    self.loginButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.loginButton.layer.cornerRadius = 25;
}

#pragma mark - Actions

- (IBAction)loginAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if (!loginEnabled) {
        [self showPrivacyPolicyAgreement];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } else {
       
        [self tryToLogin];
    }
}

-(void)tryToLogin {
    
    if ([self areInputFieldsValidForActionType:signup]) {
        
        NSString* loginString = [NSString stringWithFormat:@"+44%@", self.mobileNumber.text];
        
        [[ApiManager shared] signupWithFirstname:self.firstnameTextfield.text lastname:self.lastnameTextfield.text email:loginString completionBlock:^(id responce, NSError *error) {

            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            if (!error  && [(NSNumber*)responce[@"ok"] boolValue] ) {
                
                NSString* token = responce[@"data"][@"token"];
                BOOL inviteEnabled =  [(NSNumber*)responce[@"ok"] boolValue];
                
                [UserManager shared].currentUser.token = token;
                [UserManager shared].currentUser.inviteEnabled = inviteEnabled;
                
                [[UserManager shared].currentUser save];
                
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"UserSecondLogin"]) {
                    
                    [self performSegueWithIdentifier:FromLoginToMain sender:self];
                }else {
                    
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"UserSecondLogin"];
                    [self performSegueWithIdentifier:LoginFriendsist sender:self];
                    
                }

            } else if (error) {
                
                NSError *loginError = [[NSError alloc] initWithDomain:@"35.166.91.188" code:0 userInfo:@{NSLocalizedDescriptionKey : error.localizedDescription}];
                
                [Utils showAlertWithError:loginError fromViewController:self];
            } else {
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentUser"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSError *loginError = [[NSError alloc] initWithDomain:@"35.166.91.188" code:0 userInfo:@{NSLocalizedDescriptionKey :responce[@"msg"]}];
                
                [Utils showAlertWithError:loginError fromViewController:self];
            }
        }];
        
        if([Utils isJailBroken]) {
            
            NSError *isJailBrokenError = [[NSError alloc] initWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey :@"Apologies, our application does not run on rooted devices"}];
            
            [Utils showAlertWithError:isJailBrokenError fromViewController:self];
        }
    } else {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
}

-(void)showPrivacyPolicyAgreement {
    
    loginEnabled = YES;
    
    [[NSUserDefaults standardUserDefaults] setBool:loginEnabled forKey:@"loginEnabled"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *urlAddress = @"http://home.bt.com/pages/navigation/privacypolicy.html";
    
    SFSafariViewController * safaryVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:urlAddress]];
    [self presentViewController: safaryVC animated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if ([textField isEqual:self.mobileNumber]) {
        
        [self moveWholeViewTo:keyboardHeight];
    } else {
        [self moveWholeViewTo:[self getOffset]];
    }
}

- (BOOL) textFieldShouldReturn:(UITextField*) textField {
    
    if (self.mobileNumber.text.length > 0 && self.firstnameTextfield.text.length > 0 && self.lastnameTextfield.text.length > 0) {
        [self tryToLogin];
    } else {
        if(textField == self.firstnameTextfield) {[self.lastnameTextfield becomeFirstResponder];}
        else if(textField == self.lastnameTextfield) {[self.mobileNumber becomeFirstResponder];}
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.mobileNumber) {
        
        if(textField.text.length == 10)
        {
            if(range.length == 0)
                return NO;
        }
        
        if (textField.text.length < 13) {
            
            number = [self formatNumber:self.mobileNumber.text];
            
            textField.text = number;
        }
    }
    
    return YES;
}

#pragma mark - Helpers

- (NSString *)formatNumber:(NSString *)mobileNumber {
    
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"-" withString:@""];
    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"+" withString:@""];
//    mobileNumber = [mobileNumber stringByReplacingOccurrencesOfString:@"44" withString:@""];
    
    return mobileNumber;
}

- (BOOL)areInputFieldsValidForActionType:(ActionType)actionType {
    
    BOOL valid = NO;
    
    if (actionType == login) {
        
        if (self.mobileNumber.text.length > 0) {
            
            valid = YES;
        }
        
    } else {
        
        if (self.mobileNumber.text.length > 0 && self.firstnameTextfield.text.length > 0 && self.lastnameTextfield.text.length > 0) {
            
            valid = YES;
        }
    }
    
    if (!valid) {
        
        NSError *serverError = [[NSError alloc] initWithDomain:@"35.166.91.188" code:0 userInfo:@{NSLocalizedDescriptionKey : @"Please enter a valid data"}];
        
        [Utils showAlertWithError:serverError fromViewController:self];
    }
    
    return valid;
}


- (void)tryToRegisterPushToken {
    
    NSString *fcmToken = [FIRMessaging messaging].FCMToken;
    
    if (fcmToken && [UserManager shared].currentUser.number) {
        
        [[ApiManager shared] registerPushToken:fcmToken completionBlock:nil];
    }
}

#pragma mark - Keyboard Handling

-(CGFloat)getOffset {
    
    CGRect position = [self.view convertRect:self.mobileNumber.frame fromView:[self.mobileNumber superview]];
    CGRect viewFrame = self.view.frame;
    
    CGFloat newOffset = (position.origin.y + position.size.height) - viewFrame.size.height + keyboardHeight;
    
    if (newOffset > 0) {
        
        return newOffset + 15.0;
    } else {
        
        return 0.0;
    }
}

-(void)moveWholeViewTo:(CGFloat)height {
    [UIView animateWithDuration:0.25 animations:^{
        CGRect viewFrame = self.view.frame ;
        viewFrame.origin.y = -height;
        self.view.frame = viewFrame;
    }];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    keyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    if ([self.mobileNumber isFirstResponder]) {
        
        [self moveWholeViewTo:keyboardHeight];
    } else {
        
        [self moveWholeViewTo:[self getOffset]];
    }
}

-(void)keyboardWillHide:(NSNotification *)notification {
    
    [UIView animateWithDuration:0.25 animations:^{
        
        CGRect viewFrame = self.view.frame ;
        viewFrame.origin.y = 0.0f;
        self.view.frame = viewFrame;
    }];
}

-(void)willEnterForeground:(NSNotification *)notification {
    
    [self.view endEditing:YES];
}

@end
