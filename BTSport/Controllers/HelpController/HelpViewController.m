//
//  HelpViewController.m
//  Watch Together
//
//  Created by Артём Гуральник on 10/10/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController () {
    
    int pageNumber;
}

@property (weak, nonatomic) IBOutlet UILabel *buttonLabel;
@property (weak, nonatomic) IBOutlet UIImageView *helpImage;

@end

@implementation HelpViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
   
    pageNumber = 0;
    
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
    [UINavigationController attemptRotationToDeviceOrientation];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (IBAction)actionButton:(id)sender {
    
    if (pageNumber == 0) {
        
        pageNumber = pageNumber + 1;
        
        self.helpImage.image = [UIImage imageNamed:@"helpPage2"];
    }else if (pageNumber == 1){
        
        pageNumber = pageNumber +1;
        
        self.helpImage.image = [UIImage imageNamed:@"helpPage3"];
        self.buttonLabel.text = @"FINISH";
    }else {
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)skipAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
