//
//  StreamWatchController.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface StreamWatchController : UIViewController

@property (nonatomic, strong) NSString* videoStreamId;
@property (nonatomic, strong) NSString* channelName;

- (void)handleNotificationIfNeeded;

@end
