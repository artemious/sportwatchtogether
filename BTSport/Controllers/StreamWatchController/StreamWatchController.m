//
//  StreamWatchController.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//

#import "StreamWatchController.h"
#import "NavigationController.h"
#import "WebRTCUserCollectionViewCell.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "WatchTogetherManager.h"
#import "BroadcastingControlsView.h"
#import "HistoryController.h"
#import "UserManager.h"
#import "ApiManager.h"
#import "InvitesManager.h"
#import "HistoryManager.h"
#import "Utils.h"
#import "Constants.h"
#import <MBProgressHUD.h>
#import <WebRTC/RTCMediaStream.h>
#import <WebRTC/RTCDataChannel.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "HelpViewController.h"
#import "Contact.h"
#import "AttemtToInvitrViewController.h"
#import "RSA.h"

@interface StreamWatchController () <WatchTogetherManagerDelegate, BroadcastingControlsViewDelegate, MFMessageComposeViewControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NBMRendererDelegate, WebRTCUserCollectionViewCellDelegate, AVAssetResourceLoaderDelegate, AttemtToInvitrViewControllerDelegate>
{
    int hideInterval;
    NSTimer* hideTimer;
    NSTimer* healthCheckTimer;
    NSTimer* showTime;
    NSTimer* attemtInvite;
    BOOL volumePreferencesShown;
    double streamCurrentTime;
    double oldServerTime;
}

@property (weak, nonatomic) IBOutlet UICollectionView *userStreamsCollection;
@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (weak, nonatomic) IBOutlet UIView *gestureView;

@property (nonatomic, strong) AVPlayerViewController *playerViewController;
@property (nonatomic, strong) id<NBMRenderer> localRenderer;
@property (nonatomic, strong) id<NBMRenderer> guestRenderer;

// Controls

@property (weak, nonatomic) IBOutlet BroadcastingControlsView *broadcastingControlsView;
@property (weak, nonatomic)  UIButton *muteGuestButton;
@property (weak, nonatomic) IBOutlet UISlider *streamVolumeSlider;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoImage;

// Images UI

@property (weak, nonatomic) IBOutlet UIImageView *soundImage;

// Watch manager

@property (nonatomic, strong) WatchTogetherManager *watchTogetherManager;

// Helpers variables

@property(strong, nonatomic) NSMutableDictionary *viewDictionary;
@property(strong, nonatomic) NSMutableArray *peerArray;
@property (nonatomic) BOOL shouldReconnect;

//Hidden labels
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UILabel *hiddenLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondHiddenLabel;

@end

@implementation StreamWatchController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];

    [self.hiddenLabel setHidden:YES];
    
    if ([UserManager shared].currentUser.userPeers.count > 0) {
        
        [self.hiddenLabel setHidden:YES];
        [self.secondHiddenLabel setHidden:YES];
        
        [attemtInvite invalidate];
        attemtInvite = nil;
        
    }else {
        
        attemtInvite =  [NSTimer scheduledTimerWithTimeInterval:15.0 repeats:NO block:^(NSTimer * _Nonnull timer) {
            
            [self showAttemtInvite];
        }];
        [self.hiddenLabel setHidden:NO];
        [self.secondHiddenLabel setHidden:NO];
    }
    
    if (![UserManager shared].currentUser.firstLogin) {
        
        [UserManager shared].currentUser.firstLogin = YES;
        [[UserManager shared].currentUser save];
        
        HelpViewController* helpVC = [HelpViewController new];
        
        [self presentViewController:helpVC animated:YES completion:nil];
    }

    volumePreferencesShown = NO;
    
    [self hideShowBottomControls];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backButtonPressed:) name: UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resumeVideo) name: UIApplicationWillEnterForegroundNotification object:nil];
    
    self.userStreamsCollection.delegate = self;
    
    self.navigationItem.hidesBackButton = NO;
    
    //    self.playerView.delegate = self;

    [self performVideoStream:self.videoStreamId];

    self.watchTogetherManager = [[WatchTogetherManager alloc] initWithDelegate:self];
    
    [_soundImage setTintColor:[UIColor whiteColor]];
    
    [_streamVolumeSlider setThumbImage:[UIImage imageNamed:@"ic_custom_slider_thumb"] forState:UIControlStateNormal];
    
    [self.watchTogetherManager startLocalVideo];
    
    [self initialConfigurations];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self configureNavigation];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
    
    [UINavigationController attemptRotationToDeviceOrientation];

    [self.playerViewController.player play];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskLandscapeRight;
}

-(void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self handleNotificationIfNeeded];
    
    while ([UIDevice currentDevice].generatesDeviceOrientationNotifications) {
        
        [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    
    [_playerViewController.player pause];

    while (![UIDevice currentDevice].generatesDeviceOrientationNotifications) {
        
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    }
    
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
    [UINavigationController attemptRotationToDeviceOrientation];
    
    [super viewWillDisappear:animated];
}

#pragma mark - Configurations

- (void)initialConfigurations {
    
    self.navigationItem.hidesBackButton = YES;

    self.broadcastingControlsView.delegate = self;
    
}

#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.viewDictionary count];
}

-(UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"userCell";
    
    WebRTCUserCollectionViewCell* streamCell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    [streamCell.muteButton setHidden: YES];
    
    if (indexPath.row == 0) {
        
        if (self.localRenderer.rendererView) {
            
            //            CGFloat scaleCoeef = 1;//self.view.bounds.size.height/self.view.bounds.size.width;
            
            //            self.localRenderer.rendererView.transform = CGAffineTransformMakeScale(scaleCoeef, scaleCoeef);
            
            [streamCell displayRenderView:self.localRenderer.rendererView];
        }
        
        return streamCell;
    }else {
        
        //        CGFloat scaleCoeef = 1;//self.view.bounds.size.height/self.view.bounds.size.width;
        
        UIView *guestView = [self.viewDictionary objectForKey: [self.peerArray objectAtIndex:indexPath.row]];
        
        //        guestView.transform = CGAffineTransformMakeScale(scaleCoeef, scaleCoeef);
        
        [guestView.maskView addSubview:self.muteGuestButton];
        
        [streamCell displayRenderViewForGuest: guestView];
        
        [streamCell.muteButton setHidden: NO];
        
        streamCell.delegate = self;
        
        return streamCell;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(self.userStreamsCollection.bounds.size.width, self.userStreamsCollection.bounds.size.height/4);
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:nil completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        
        [self.userStreamsCollection performBatchUpdates:^{
            
            [self.userStreamsCollection setCollectionViewLayout:self.userStreamsCollection.collectionViewLayout animated:YES];
            [self.userStreamsCollection reloadData];
        } completion:nil];
    }];
}

#pragma mark - Configurations

-(void)configureNavigation {
    
    self.navigationItem.hidesBackButton = NO;

    self.broadcastingControlsView.delegate = self;
    self.broadcastingControlsView.hidden = NO;
}

#pragma mark - Actions

- (IBAction)increaseVolume:(UISlider *)sender {
    
    [_playerViewController.player setVolume:sender.value];
    [_playerViewController.player play];
    
    volumePreferencesShown = NO;
    
    [self hideShowBottomControls];
    
    if (sender.value == 0) {
        
        [_soundImage setTintColor:[UIColor grayColor]];
    }else {
        
        [self.soundImage setImage: [UIImage imageNamed: @"ic_sound"]];
        [_soundImage setTintColor:[UIColor whiteColor]];
    }
}

- (IBAction)backButtonPressed:(id)sender {
    
    [self shouldDisconnect];

    hideTimer = nil;
    healthCheckTimer = nil;
    showTime  = nil;
    
    [self.playerViewController.player pause];
    
    self.playerView = nil;
    self.playerViewController = nil;
    
    [UserManager shared].currentUser.roomToConnectTo = [UserManager shared].currentUser.roomID;
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)shareAction:(id)sender {
    
    [attemtInvite invalidate];
    attemtInvite = nil;
    
    [self performSegueWithIdentifier:StreamWatchToHistory sender:self];
}

- (IBAction)wathcAction:(id)sender {
    
    self.watchTogetherManager = [[WatchTogetherManager alloc] initWithDelegate:self];
    [self shouldToggleVideo];
    
    [self.watchTogetherManager startLocalVideo];
}

#pragma mark - Helpers

- (void)showInvitePage {
    
    [self shareAction:self];
}

-(void) showAttemtInvite {
    
    [attemtInvite invalidate];
    attemtInvite = nil;
    
    if ([UserManager shared].currentUser.userPeers.count == 0) {
        
        AttemtToInvitrViewController* attemptVC = [AttemtToInvitrViewController new];
        attemptVC.delegate = self;
        
        [self presentViewController:attemptVC animated:YES completion:nil];
    }
}

-(void) checkOrientation {
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeRight) forKey:@"orientation"];
    
    [UINavigationController attemptRotationToDeviceOrientation];
}

- (id<NBMRenderer>)rendererForStream:(RTCMediaStream *)stream {
    
    NSParameterAssert(stream);
    
    id<NBMRenderer> renderer = nil;
    
    RTCVideoTrack *videoTrack = [stream.videoTracks firstObject];
    
    NBMRendererType rendererType = [NBMMediaConfiguration defaultConfiguration].rendererType;
    
    if (rendererType == NBMRendererTypeOpenGLES) {
        
        renderer = [[NBMEAGLRenderer alloc] initWithDelegate:self];
    }
    
    renderer.videoTrack = videoTrack;
    
    return renderer;
}

- (void)stopWatching {
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    
    [self.watchTogetherManager stopWatching:nil];
    
    if (self.localRenderer) {
        
        self.localRenderer = nil;
    }
    
    if (self.guestRenderer) {
        
        self.guestRenderer = nil;
    }
    
    self.watchTogetherManager = nil;
}

- (void)acceptInvitation {
    
    [self checkOrientation];
    
    WatchInvite *watchInvite = [[InvitesManager shared] getUnhandledWatchInvite];
    
    [UserManager shared].currentUser.roomToConnectTo = watchInvite.getRoomID;
    
    watchInvite.isHandled = YES;
    
    if (self.localRenderer == nil) {
        
        [self wathcAction:nil];
        
    } else {
        
        self.shouldReconnect = YES;
        
        [self stopWatching];
        
        [UserManager shared].currentUser.roomToConnectTo = watchInvite.getRoomID;
        
        if (watchInvite.getVideoID){
            
            [self performVideoStream:watchInvite.getVideoID];
        }
        
        self.watchTogetherManager = [[WatchTogetherManager alloc] initWithDelegate:self];
        [self.navigationItem setTitle:watchInvite.getChannelName];
    }
}

-(void)performVideoStream:(NSString*)videoID {

    NSString *privkey = @"-----BEGIN RSA PRIVATE KEY-----MIIEpQIBAAKCAQEA4RuuxtUgN/Nga5mTXxZI22mskBEqj1R8AEFfFRFVEK/iKxW77F9xsGJEDJSPOysURury+mKrazAPP/+A70GGsZjS1UBEQyB//li9JMlgdF5o8z6r1FCZWASDv4xSyhuiK0qQcBQYsP4PDlV6yxLQueolBG3GIrBr9STXxyX5enX+qwJN/tERtKXWbJf5W+zHoVK+XSI23W7y0emArKb9ufJ5df/YvNwUtr8DN2+eHn2yuaxpuCBtLACwYWwxBMIsTTXKCFSxgTbrWd3jaB6y5wAVzcDNDYVtdh0caXnWw3FkSpytIaGAxj9vWteF6NybC0lKCuyjlFg+LhHPgYJ9QQIDAQABAoIBAQCmQX7VRUVVbX/SGIwal2fZ/XNAuBz53Qj0W1OxSKrN90kQTy4LRva9seXWcMZUid2g/xfTy5oVQVqSBtAgZQHpGxjj/WItPcyqerm/eKevaqcSuD/KQHjw4+y2RRpEQSKF3L1Oudj+p7VQ/dsCLvRPy1npqnXC2w/w73QalrZqNWZwLKjBrJU+pwVincEuQ3dC4HqfY7r9heoZRc9s39XcAuPXQjQWD6+9vVxkonqeSgl+UOzthQ+jZnl+WILRu7qmrI9bSTAIEdH5We+cAdPHe51zA+SMJqbhNGL5g0nfO3gRVYDiAwb9rq4X04a7X0xbzEAgOE1KR0cDef1UsCa9AoGBAP2E6e7iydjYo1enqcPnbRykbeydSallxTbOBh5XEAzIaL9pbXc+BS0tw49XIltM5yqA+rI7WuveJ3JPRYapu9u/aUVJs3/pRrbs1dCp/tmNObXDJkd8qAJgR61iHBBnpAdRdXZZUJ5TEmJ3h3ff6og2WCyVmRcQPl6BdG6JJ4ILAoGBAONPmM7ns/rnVlRtkpGRI97ZasT2Iog5ii7EoLlcKuvoxTDioATn9Uegm3qirQa7f3vu9CkHGn323x/TEI6OQBVmgESUhuPkev8iPcRBWdqU1DjQvaydIllHOcIpIn08hYxr2Io/54YYnuVNjFMSZZG+kl6e5n5oSfQuRde+zHljAoGAfQVR09OVVNGo4640Iv76qPhRmTELxXUBp4A8NjQbHyKpvVNBf19fcCM0YHjI4ZtwGF7YwpJQySaZ7142XC1SDhE72cZ5taeVjtoGq0tZ+XESt5cQRoqr09glINTUICbsFgvO0FlFT36J/3MQpEYRQ7EBFKOFhoElMLfIckZC7HcCgYEAunhXgJQu3wrblRL2A5gu9ucgYUcQ+PNK2ibPOn/TbgdbjEi8wImQexvTr/y3OCij4gUaInYDU4Yrb63zjR5Aj82wN5IoPdb210k9pjt9/IPOvHTwxCN7RAXb6d1Yv6fgXlsyHedihGy6HJ1LGfKjg7Xz2M89gkEHOdqjzOJmVsUCgYEAxjfXOw9Nl3yB7YRqdGB+VaAuns1RRQNfvt8PNB7v9a2fNMAcKZRdvAZ5QgVj5slsN21JYcBdB/8zlDkw4VBB/LlkVevr6Z2WQgzOzeUwgc609h/BSLUt2Y1k0uhnzDju+3ApMv4N327Eipv65rpQT8dpMWSxHlOqhq9gqQIgghQ=-----END RSA PRIVATE KEY-----";
    
    NSData* decodeData = [[NSData alloc] initWithBase64EncodedString:videoID  options:NSUTF8StringEncoding];
    
    NSString* streamURL = [[NSString alloc] initWithData:[RSA decryptData:decodeData privateKey:privkey] encoding:NSUTF8StringEncoding];
    
    NSURL *videoURL = [NSURL URLWithString:videoID];
    AVURLAsset* asset  = [AVURLAsset URLAssetWithURL:videoURL options:nil];
    
    [asset.resourceLoader setDelegate:self queue:dispatch_get_main_queue()];

    AVPlayerItem* playerItem = [AVPlayerItem playerItemWithAsset:asset automaticallyLoadedAssetKeys:@[@"duration"]];

    AVPlayer *player = [[AVPlayer alloc] initWithPlayerItem:playerItem];
    player.volume = self.streamVolumeSlider.value;

    _playerViewController = [AVPlayerViewController new];
    _playerViewController.player = player;
    _playerViewController.showsPlaybackControls = NO;

    [_playerViewController.player.currentItem addObserver:self forKeyPath:@"status" options:0 context:nil];
//    [_playerViewController.player.currentItem setPreferredMaximumResolution:CGSizeMake(640, 360)];
    [_playerViewController.player.currentItem setCanUseNetworkResourcesForLiveStreamingWhilePaused:YES];
    [_playerViewController.player.currentItem setPreferredForwardBufferDuration:0];

    [self addChildViewController:_playerViewController];

    _playerViewController.view.frame = self.playerView.frame;

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideShowBottomControls)];

    [self.gestureView addGestureRecognizer:tap];
    [self.playerView addSubview:_playerViewController.view];

    self.view.autoresizesSubviews = YES;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resumeVideo) name:AVPlayerItemPlaybackStalledNotification object:self.playerViewController.player.currentItem];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resumeVideo) name:AVPlayerItemFailedToPlayToEndTimeNotification object:self.playerViewController.player.currentItem];

    [self startHealthCheckTimer];
}

-(void) resumeVideo {

    [healthCheckTimer invalidate];

    [self performVideoStream:self.videoStreamId];

    [_playerViewController.player play];
    
    NSLog(@"Video reload froma failure");
}

-(void) hideShowBottomControls {
    
    if (hideTimer) {
        
        [hideTimer invalidate];
        hideTimer = nil;
    }
    
    if (volumePreferencesShown) {
        
        [self.streamVolumeSlider setHidden:YES];
        [self.soundImage setHidden:YES];
        [self.logoImage setHidden:YES];
        [self.backButton setHidden:YES];
    }else {
        
        [self.streamVolumeSlider setHidden:NO];
        [self.soundImage setHidden:NO];
        [self.logoImage setHidden:NO];
        [self.backButton setHidden:NO];
        
        hideTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 repeats:NO block:^(NSTimer * _Nonnull timer) {
            
            [self hideShowBottomControls];
        }];
    }
    
    volumePreferencesShown = !volumePreferencesShown;
}

- (void)startHealthCheckTimer {
    
    [self healthCheckTimerDidFire]; // fires once immediately
    healthCheckTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                             target:self
                                                           selector:@selector(healthCheckTimerDidFire)
                                                           userInfo:nil
                                                            repeats:YES];
    
    showTime = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                target:self
                                              selector:@selector(getTimeTimerAction)
                                              userInfo:nil
                                               repeats:YES];
}

- (void)healthCheckTimerDidFire {

    [self.playerViewController.player.currentItem.asset loadValuesAsynchronouslyForKeys:@[@"duration"]
                                                                      completionHandler:nil];
    
    
    
    if ([[UserManager shared].currentUser.currentRoom isEqualToString:[UserManager shared].currentUser.roomID]) {
        
        [self.watchTogetherManager checkTimer:[NSNumber numberWithDouble:streamCurrentTime]];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"status"]) {
        
        switch (_playerViewController.player.currentItem.status) {
                
            case AVPlayerItemStatusReadyToPlay:
                
                [self.playerViewController.player pause];
                [self.playerViewController.player play];
                
                NSLog(@"Reload");
                
                break;
                
            case AVPlayerItemStatusFailed:
                
                [self resumeVideo];
                
                 NSLog(@"Reload from failure");
                
                break;
            default:
                break;
        }
    }
}

- (void) getTimeTimerAction {
    
    streamCurrentTime = [self getCurrentTimeDateState];
}

- (double) getCurrentTimeDateState {
    
    double dateSec = [self.playerViewController.player.currentItem.currentDate timeIntervalSince1970] * 1000;
    
//commented for show on lable when need to show time of stream
    
    NSDateFormatter * fmt = [NSDateFormatter new];
    [fmt setDateStyle:NSDateFormatterFullStyle];
    [fmt setTimeStyle:NSDateFormatterFullStyle];
    [fmt setDateFormat:@"HH:mm:ss.SSS"];

    NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:[self.playerViewController.player.currentItem.currentDate timeIntervalSince1970]];
    NSString * s = [fmt stringFromDate:date];

    self.timeLabel.text = s;
    
    return (long)dateSec;
}

#pragma mark - WebRTCUserCollectionViewCellDelegate

- (void)webRTCUserCell:(WebRTCUserCollectionViewCell *)cell enableAudio:(BOOL)enable {
    
    NSIndexPath* selectedIndexPath = [_userStreamsCollection indexPathForCell:cell];
    NSString* peerIdentifier = self.peerArray[selectedIndexPath.row];
    NBMPeer* peer = [self.watchTogetherManager remotePeerWithIdentifier:peerIdentifier];
    
    [self.watchTogetherManager toggleAudioFromPeer:peer completion:^(BOOL enabled) {
        
        if(enable) {
            
            [cell.muteButton setImage:[UIImage imageNamed:@"ic_bottom_mic_off"] forState:UIControlStateNormal];
        }else {
            
            [cell.muteButton setImage:[UIImage imageNamed:@"ic_bottom_mic_on"] forState:UIControlStateNormal];
        }
    }];
}

#pragma mark - MFMessageComposeViewControllerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

# pragma mark - Check for invitation

- (void)handleNotificationIfNeeded {
    
    __block WatchInvite *watchInvite = [[InvitesManager shared] getUnhandledWatchInvite];
    
    if (watchInvite && !self.presentedViewController) {
        
        NSString *message = [watchInvite getInvitationMessage];
        
        UIAlertController *invitationAlert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
        
        [invitationAlert addAction:[UIAlertAction actionWithTitle:@"Join" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self acceptInvitation];
            
        }]];
        
        [invitationAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [[ApiManager shared] logOpenLinkAction:watchInvite.getBranchLink completionBlock:nil];
            
            watchInvite.isHandled = YES;
            
        }]];
        
        [self presentViewController:invitationAlert animated:YES completion:nil];
    }
}

#pragma mark - WatchTogetherManagerDelegate

- (void)watchManagerDidFinish {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (self.shouldReconnect) {
        
        self.shouldReconnect = NO;
        
        [self wathcAction:nil];
        
    } else {
        
        [UserManager shared].currentUser.currentRoom = nil;
        [UserManager shared].currentUser.roomToConnectTo = [UserManager shared].currentUser.roomID;
    }
}

- (void)watchManagerDidAddLocalStream:(RTCMediaStream *)localStream {
    
    if (self.localRenderer) {
        
        [self.localRenderer.rendererView removeFromSuperview];
        
        self.localRenderer = nil;
    }
    
    id<NBMRenderer> renderer = [self rendererForStream:localStream];
    
    self.localRenderer = renderer;
    
    [self.watchTogetherManager startWatching];
    
    if (self.localRenderer) {
        
        [self.userStreamsCollection reloadData];
        
        self.peerArray = [[NSMutableArray alloc] init];
        
        self.viewDictionary = [[NSMutableDictionary alloc]init];
    }
    
    [UserManager shared].currentUser.currentRoom = [UserManager shared].currentUser.roomToConnectTo;
//    [UserManager shared].currentUser.roomToConnectTo = [UserManager shared].currentUser.roomID;
}

- (void)watchManagerDidRemoveLocalStream:(RTCMediaStream *)localStream {
    
}

- (void)watchManagerPeerJoined:(NBMPeer *)peer {
    
    [[HistoryManager shared] updateHistoryList];
}

- (void)watchManagerPeerLeft:(NBMPeer *)peer {
    
    [self.peerArray removeObject:peer.identifier];
    
    [self.viewDictionary removeObjectForKey: peer.identifier];
    
    [self.userStreamsCollection reloadData];

}

- (void)watchManagerRoomJoined:(NSError *)error {
    
    [UIApplication sharedApplication].idleTimerDisabled = NO; // https://stackoverflow.com/a/29256946/3135344
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    
    if (!error) {
        
        for (NBMPeer *peer in self.watchTogetherManager.remotePeers) {
            
            [[HistoryManager shared] updateHistoryList];
        }
        
        self.broadcastingControlsView.hidden = NO;
        
    } else {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
        
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)watchManagerDidAddStream:(RTCMediaStream *)remoteStream ofPeer:(NBMPeer *)remotePeer {
    
    if (self.guestRenderer) {
        
        self.guestRenderer = nil;
    }
    
    if (remotePeer && remoteStream) {
        
        id<NBMRenderer> renderer = [self rendererForStream:remoteStream];
        
        self.guestRenderer = renderer;
        
        [self.watchTogetherManager receiveAudioFromPeer:remotePeer completion:^(BOOL success) {
            
        }];
        
//        [self.watchTogetherManager receiveVideoFromPeer:remotePeer completion:^(NSError *error) {
//            
//        }];
        
        [self.peerArray addObject: remotePeer.identifier];
        
        [self.viewDictionary setObject:self.guestRenderer.rendererView forKey: remotePeer.identifier];
        [self.userStreamsCollection reloadData];
    }
}

- (void)watchManagerDidFailWithError:(NSError *)error {
    
    NSString* currentError = nil;
    
    if ( [error.localizedDescription isEqualToString:@"Room is full"] ) {
        
        currentError = @"Sorry but your friend's watch together session is full now. There is only space on the screen for 4 friends. Feel free to start your own session by picking something to watch and inviting your friends.";
    }else {
        
        currentError = error.localizedDescription;
    }
    NSLog(@"%@", error);
//    [UserManager shared].currentUser.currentRoom = nil;
    [UserManager shared].currentUser.roomToConnectTo = [UserManager shared].currentUser.roomID;
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:currentError preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)watchManagerDidGetStreamTime:(NSNumber *)time {
    
    double delta = [time doubleValue]  -  streamCurrentTime;
    
    double timeServ = ([time doubleValue] + 200 ) / 1000;
    
    NSLog(@"%f need seek", delta);
    
    if(!oldServerTime) {
        
        oldServerTime = [time doubleValue];
    }
    
    CMTime currrent = self.playerViewController.player.currentItem.currentTime;

    if (oldServerTime != [time doubleValue]) {

        if (delta > 500 ) {

            CMTime timeToAdd;
            
            if(delta < 5000 && delta >= 1000) {
                
                timeToAdd = CMTimeMakeWithSeconds(1, 1);
                CMTime timeToSeek = CMTimeAdd(currrent, timeToAdd);
                
                [self.playerViewController.player.currentItem seekToTime:timeToSeek];
            }else if (delta < 1000) {
                
                [self.playerViewController.player.currentItem seekToDate:[NSDate dateWithTimeIntervalSince1970: (long)timeServ]];
            } else {
                
                timeToAdd = CMTimeMakeWithSeconds(3, 1);
                CMTime timeToSeek = CMTimeAdd(currrent, timeToAdd);

                [self.playerViewController.player.currentItem seekToTime:timeToSeek];
            }
        } else if (delta < -500){

            [self.playerViewController.player.currentItem seekToDate:[NSDate dateWithTimeIntervalSince1970: (long)timeServ]];

        }
    }
}

#pragma mark - BroadcastingControlsViewDelegate

- (void)shouldToggleAudio {
    
    [self.watchTogetherManager enableAudio:!self.watchTogetherManager.isAudioEnabled];
    
    if (self.watchTogetherManager.isAudioEnabled) {
        
        [self.broadcastingControlsView.audioButton setImage:[UIImage imageNamed:@"ic_bottom_mic_on"] forState:UIControlStateNormal];
        
    } else {
        
        [self.broadcastingControlsView.audioButton setImage:[UIImage imageNamed:@"ic_bottom_mic_off"] forState:UIControlStateNormal];
    }
}

- (void)shouldToggleVideo {
    
    [self.watchTogetherManager enableVideo:!self.watchTogetherManager.isVideoEnabled];
    
    if (self.watchTogetherManager.isVideoEnabled) {
        
        [self.broadcastingControlsView.videoButton setImage:[UIImage imageNamed:@"ic_bottom_video_on"] forState:UIControlStateNormal];
        
    } else {
        
        [self.broadcastingControlsView.videoButton setImage:[UIImage imageNamed:@"ic_bottom_video_off"] forState:UIControlStateNormal];
    }
}

- (void)shouldDisconnect {
    
    [self stopWatching];
}
#pragma mark - NBMRendererDelegate

- (void)renderer:(id<NBMRenderer>)renderer streamDimensionsDidChange:(CGSize)dimensions {
    
    // Handle ?
}

- (void)rendererDidReceiveVideoData:(id<NBMRenderer>)renderer {
    
    // Handle ?
}

#pragma mark - Public

- (void)setAudioEnabled:(BOOL)enabled forPeer:(NBMPeer*)peer {
    
}

#pragma mark - Touches Handling

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event  {
    
    UITouch *touch = [touches anyObject];
    
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:StreamWatchToHistory]) {
        
        HistoryController *historyViewController = (HistoryController*)segue.destinationViewController;
        
        historyViewController.currentFriends = [NSMutableArray arrayWithArray:[UserManager shared].currentUser.userSereverContacts];
        
        for (Contact* currentContact in [UserManager shared].currentUser.userSereverContacts) {
            
            for (NBMPeer* currentPeer in [UserManager shared].currentUser.userPeers) {
                
                if ([currentPeer.emailNumberHash isEqualToString:currentContact.deviceIdHash]) {
                    
                    [historyViewController.currentFriends removeObject:currentContact];
                }
            }
        }
        
        historyViewController.videoStreamId = self.videoStreamId;
        historyViewController.channelName = self.channelName;
    }
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
