//
//  ContactsOnMainScreenViewController.m
//  Watch Together
//
//  Created by Артём Гуральник on 10/1/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import "ContactsOnMainScreenViewController.h"
#import "Contact.h"
#import "MyContactTableViewCell.h"
#import "ApiManager.h"
#import "UserManager.h"
#import "ContactsManager.h"


@interface ContactsOnMainScreenViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) UIRefreshControl* refreshControl;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;

@end

@implementation ContactsOnMainScreenViewController

-(NSMutableArray *) friendsList {
    
    if ([self.searchTextField.text isEqualToString:@""]) {
        return [UserManager shared].currentUser.userSereverContacts;
    } else {
        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.name contains[cd] %@", self.searchTextField.text];
        NSArray * contacts = [[UserManager shared].currentUser.userSereverContacts filteredArrayUsingPredicate: bPredicate];
        NSMutableArray * array = [[NSMutableArray alloc] initWithArray:contacts];
        return array;
    }
}

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.searchTextField.delegate =self;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    UINib* ContactCell = [UINib nibWithNibName:NSStringFromClass([MyContactTableViewCell class])
                                        bundle:nil];
    
    [self.tableView registerNib:ContactCell forCellReuseIdentifier:@"userContact"];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.tableView setAllowsSelection:NO];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    
    [self.refreshControl addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshControl];
}

-(void)dismissKeyboard {
    
    [self.searchTextField resignFirstResponder];
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyContactTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"userContact" forIndexPath:indexPath];
    
    if ( self.friendsList.count > 0) {
        
        Contact* currentContact  = self.friendsList[indexPath.row];
        
        [cell popuplateCellWith:currentContact];
    }
    
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.friendsList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return self.tableView.bounds.size.height / 6;
}

-(void) refreshAction {
    
    [self.refreshControl beginRefreshing];
    
    [ContactsManager getFriends:self and:^{
        [self.refreshControl endRefreshing];
        [self.tableView reloadData];
        
        if (![User getSavedUser]) {
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }];
    
}

- (IBAction)searchTextFieldTextChanged:(UITextField *)sender {
    
    [self.tableView reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField endEditing:YES];
    
    return YES;
}
@end
