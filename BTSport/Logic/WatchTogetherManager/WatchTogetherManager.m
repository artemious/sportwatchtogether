//
//  WatchTogetherManager.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "WatchTogetherManager.h"

#import "UserManager.h"

#import <WebRTC/WebRTC.h>

#import "Utils.h"

//static NSString *kRoomURL = @"https://rs.ccc/room";
static NSString *kRoomURL = @"https://sportwt.sceenic.co/room";  // @"wss://btsport.sceenic.co/room";  //http://news.watchtogether.club/room

typedef void(^ErrorBlock)(NSError *error);

@interface WatchTogetherManager () <NBMWebRTCPeerDelegate, NBMRoomClientDelegate>

@property (nonatomic, strong) NBMWebRTCPeer *webRTCPeer;
@property (nonatomic, strong) NBMRoomClient *roomClient;

@property (nonatomic, assign) BOOL loopBack;

@property (nonatomic, strong, readonly) NSSet *allPeers;

@property (nonatomic, strong) NSMutableArray *mutableRemoteStreams;

@property (nonatomic, strong, readonly) NSString *localConnectionId;

@property (nonatomic, assign) NSUInteger retryCount;

@property (nonatomic, copy) ErrorBlock publishVideoBlock;
@property (nonatomic, copy) ErrorBlock unpublishVideoBlock;

@end

@implementation WatchTogetherManager

- (instancetype)initWithDelegate:(id)delegate {
    
    self = [super init];
    
    if (self) {
        
        self.delegate = delegate;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSessionRouteChange:) name:AVAudioSessionRouteChangeNotification object:nil];
    }
    
    return self;
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Public

- (void)startLocalVideo {
    
    [self setupWebRTCSession];
}

- (void)stopLocalVideo {
    
    [self.webRTCPeer stopLocalMedia];
}

- (void)startWatching {
    
    NBMRoom *room = [[NBMRoom alloc] initWithUsername:[UserManager shared].currentUser.fullname roomName:[UserManager shared].currentUser.roomToConnectTo roomURL:[NSURL URLWithString:kRoomURL] dataChannels:false];
    
    [self setupRoomClient:room];
}

- (void)extracted:(void (^)(NSError *))block {
    [self.roomClient leaveRoom:^(NSError *error) {
        
        if (block) {
            block(error);
        }
        
        [self disconnect];
        
    }];
}

- (void)stopWatching:(void (^)(NSError *))block {
    
    [self extracted:block];
}

- (void)publishVideo:(void (^)(NSError *))block loopback:(BOOL)doLoopback {
    
    BOOL alreadyPublished = [self peerHasPublishedMedia:[self localPeer]];
    
    if (alreadyPublished) {
        
        if (block) {
            block(nil);
        }
        
        return;
    }
    
    self.loopBack = doLoopback;
    
    BOOL hasMediaStarted = self.webRTCPeer.localStream ? YES : NO;
    
    if (!hasMediaStarted) {
        
        hasMediaStarted = [self.webRTCPeer startLocalMedia];
    }
    
    if (hasMediaStarted) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (self.delegate) {
                
                [self.delegate watchManagerDidAddLocalStream:self.localStream];
            }
        });
        
        NSString *connectionId = [self localConnectionId];
        
        [self.webRTCPeer generateOffer:connectionId withDataChannels:self.roomClient.room.dataChannels completion:^(NSString *sdpOffer, NBMPeerConnection *connection) {
            
            [self.roomClient publishVideo:sdpOffer loopback:NO completion:^(NSString *sdpAnswer, NSError *error) {
                
                if (block) {
                    block(error);
                }
                
                [self.webRTCPeer processAnswer:sdpAnswer connectionId:connection.connectionId];
            }];
        }];
    } else {
        //Return error?
    }
}

- (void)unpublishVideo:(void (^)(NSError *))block {
    
    [self.webRTCPeer stopLocalMedia];
    
    if (self.delegate) {
        
        [self.delegate watchManagerDidRemoveLocalStream:self.localStream];
    }
    
    [self.webRTCPeer closeConnectionWithConnectionId:[self localConnectionId]];
    
    [self.roomClient unpublishVideo:^(NSError *error) {
        
        if (block) {
            block(error);
        }
    }];
}

- (void)receiveVideoFromPeer:(NBMPeer *)peer completion:(void (^)(NSError *error))block {
    
    NSString *connectionId = [self connectionIdOfPeer:peer];
    
    [self.webRTCPeer generateOffer:connectionId withDataChannels:self.roomClient.room.dataChannels completion:^(NSString *sdpOffer, NBMPeerConnection *connection) {
        
        [self.roomClient receiveVideoFromPeer:peer offer:sdpOffer completion:^(NSString *sdpAnswer, NSError *error) {
            
            [self.webRTCPeer processAnswer:sdpAnswer connectionId:connection.connectionId];
        }];
    }];
}

- (void)unsubscribeVideoFromPeer:(NBMPeer *)peer completion:(void (^)(NSError *))block {
    
    NSString *connectionId = [self connectionIdOfPeer:peer];
    
    [self.webRTCPeer closeConnectionWithConnectionId:connectionId];
    
    [self.roomClient unsubscribeVideoFromPeer:peer completion:^(NSString *sdpAnswer, NSError *error) {
        
        if (block) {
            block(error);
        }
    }];
}

- (void)receiveAudioFromPeer:(NBMPeer *)peer completion:(void (^)(BOOL success))block {
    
    [self.roomClient receiveAudioFromPeer:peer completion:block];
}

- (void)unsubscribeAudioFromPeer:(NBMPeer *)peer completion:(void (^)(BOOL success))block {
    
    [self.roomClient unsubscribeAudioFromPeer:peer completion:block];
}

- (void)toggleAudioFromPeer:(NBMPeer *)peer completion:(void (^)(BOOL enabled))block {
    
    NSString *connectionId = [self connectionIdOfPeer:peer];
    
    NBMPeerConnection * peerConection = [self.webRTCPeer connectionWithConnectionId:connectionId];
    
    __block BOOL isEnabled = YES;
    
    for (RTCAudioTrack *audioTrack in peerConection.remoteStream.audioTracks) {
        
        audioTrack.isEnabled = !audioTrack.isEnabled;
        
        isEnabled = audioTrack.isEnabled;
    }
    
    block(isEnabled);
}

- (void)selectCameraPosition:(NBMCameraPosition)cameraPosition {
    
    [self.webRTCPeer selectCameraPosition:cameraPosition];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self.delegate) {
            
            [self.delegate watchManagerDidAddLocalStream:self.localStream];
        }
    });
}

- (BOOL)isVideoEnabled {
    
    return [self.webRTCPeer isVideoEnabled];
}

- (void)enableVideo:(BOOL)enable {
    
    [self.webRTCPeer enableVideo:enable];
}

- (BOOL)isAudioEnabled {
    
    return [self.webRTCPeer isAudioEnabled];
}

- (void)enableAudio:(BOOL)enable {
    
    [self.webRTCPeer enableAudio:enable];
}

- (void)disconnect {
    
    [self.roomClient disconnect];
    
    [self teardownRoomClient];
    
    [self teardownWebRTCSession];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        if (self.delegate) {
            
            [self.delegate watchManagerDidFinish];
        }
    });
}

- (BOOL)isConnected {
    
    return self.roomClient.connected;
}

- (BOOL)isJoined {
    
    return self.roomClient.joined;
}

+ (NSSet *)keyPathsForValuesAffectingConnected {
    
    return [NSSet setWithObjects:@"self.roomClient.connected", nil];
}

+ (NSSet *)keyPathsForValuesAffectingJoined {
    
    return [NSSet setWithObjects:@"self.roomClient.joined", nil];
}

- (NBMPeer *)localPeer {
    
    return self.roomClient.room.localPeer;
}

- (RTCMediaStream *)localStream
{
    
    return self.webRTCPeer.localStream;
}

- (NSArray *)remotePeers {
    
    return [self.roomClient.peers copy];
}

- (NSArray *)remoteStreams
{
    return [self.mutableRemoteStreams copy];
}

- (NBMCameraPosition)cameraPosition {
    
    return self.webRTCPeer.cameraPosition;
}

- (NBMPeer*)remotePeerWithIdentifier:(NSString*)identifier {
    
    for (NBMPeer* peer in self.remotePeers) {
        if ([peer.identifier isEqualToString:identifier]) {
            return peer;
        }
    }
    
    return nil;
}

#pragma mark - NBMWebRTCPeerDelegate

- (void)webRTCPeer:(NBMWebRTCPeer *)peer didGenerateAnswer:(RTCSessionDescription *)sdpAnswer forConnection:(NBMPeerConnection *)connection {
    
}

- (void)webRTCPeer:(NBMWebRTCPeer *)peer didGenerateOffer:(RTCSessionDescription *)sdpOffer forConnection:(NBMPeerConnection *)connection {
    
    NBMPeerConnection *localConnection = [self connectionOfPeer:[self localPeer]];
    
    if ([connection isEqual:localConnection]) {
        
        [self.roomClient publishVideo:(sdpOffer.sdp) loopback:NO completion:^(NSString *sdpAnswer, NSError *error) {
            
            [self.webRTCPeer processAnswer:sdpAnswer connectionId:connection.connectionId];
        }];
    } else {
        
        NBMPeer *remotePeer = [self peerOfConnection:connection];
        
        [self.roomClient receiveVideoFromPeer:remotePeer offer:sdpOffer.sdp completion:^(NSString *sdpAnswer, NSError *error) {
            
            [self.webRTCPeer processAnswer:sdpAnswer connectionId:connection.connectionId];
        }];
    }
}

- (void)webRTCPeer:(NBMWebRTCPeer *)peer didAddStream:(RTCMediaStream *)remoteStream ofConnection:(NBMPeerConnection *)connection {
    
    [self.mutableRemoteStreams addObject:remoteStream];
    
    NBMPeer *remotePeer = [self peerOfConnection:connection];
    
    if (![remotePeer isEqual:[self localPeer]] && self.loopBack) {
        
        return;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(watchManagerDidAddStream:ofPeer:)]) {
        
        [self.delegate watchManagerDidAddStream:remoteStream ofPeer:remotePeer];
    }
}

- (void)webRTCPeer:(NBMWebRTCPeer *)peer didRemoveStream:(RTCMediaStream *)remoteStream ofConnection:(NBMPeerConnection *)connection {
    
    [self.mutableRemoteStreams removeObject:remoteStream];
    
    NBMPeer *remotePeer = [self peerOfConnection:connection];
    
    if (self.delegate && remotePeer && [self.delegate respondsToSelector:@selector(watchManagerDidRemoveStream:ofPeer:)]) {
        
        [self.delegate watchManagerDidRemoveStream:remoteStream ofPeer:remotePeer];
    }
}

- (void)webRTCPeer:(NBMWebRTCPeer *)peer didAddDataChannel:(RTCDataChannel *)dataChannel {
    
    if (self.delegate&& [self.delegate respondsToSelector:@selector(watchManagerDidAddDataChannel:)]) {
        
        [self.delegate watchManagerDidAddDataChannel:dataChannel];
    }
}

- (void)webRTCPeer:(NBMWebRTCPeer *)peer hasICECandidate:(RTCIceCandidate *)candidate forConnection:(NBMPeerConnection *)connection {
    
    NBMPeer *remotePeer = [self peerOfConnection:connection];
    
    [self.roomClient sendICECandidate:candidate forPeer:remotePeer];
}

- (void)webrtcPeer:(NBMWebRTCPeer *)peer iceStatusChanged:(RTCIceConnectionState)state ofConnection:(NBMPeerConnection *)connection {
    
    switch (state) {
        case RTCIceConnectionStateNew:
        case RTCIceConnectionStateChecking:
            break;
        case RTCIceConnectionStateCompleted:
        case RTCIceConnectionStateConnected:
            
            connection.iceAttempts = 0;
            break;
            
        case RTCIceConnectionStateCount:
        case RTCIceConnectionStateClosed:
        {
            [self.webRTCPeer closeConnectionWithConnectionId:connection.connectionId];
            break;
        }
            
        case RTCIceConnectionStateDisconnected:
        {
            if (connection.iceAttempts < 3) {
                
                NBMPeerConnection *localConnection = [self connectionOfPeer:[self localPeer]];
                
                if ([connection isEqual:localConnection]) {
                    
                    [self safeICERestartForLocalPeer];
                } else {
                    
                    [self safeICERestartForRemotePeer:[self peerOfConnection:connection]];
                }
            }
            
            break;
        }
        case RTCIceConnectionStateFailed:
        {
            [self.webRTCPeer closeConnectionWithConnectionId:connection.connectionId];
            
            if (self.connected && self.mutableRemoteStreams.count == 0 && self.delegate) {
                
                NSError *iceFailedError = [NSError errorWithDomain:@"it.nubomedia.NBMRoomManager"
                                                              code:0
                                                          userInfo:@{NSLocalizedDescriptionKey: @"Connection failed during ICE candidate phase"}];
                
                [self.delegate watchManagerDidFailWithError:iceFailedError];
                
            }
            
            break;
        }
    }
    
    NBMPeer *remotePeer = [self peerOfConnection:connection];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(watchManagerIceStatusChanged:ofPeer:)]) {
        
        [self.delegate watchManagerIceStatusChanged:state ofPeer:remotePeer];
    }
}

#pragma mark - NBMRoomClientDelegate

- (void)client:(NBMRoomClient *)client isConnected:(BOOL)connected {
    
    [[UserManager shared].currentUser.userPeers removeAllObjects];
    [[UserManager shared].currentUser.userPeers addObjectsFromArray:client.peers];
    
    if (connected) {
        
        self.retryCount = 0;
        
        if (!self.joined) {
            
            [self joinToRoom];
        }
        
    } else {
        
        [self manageRoomClientConnection];
    }
}

- (void)client:(NBMRoomClient *)client didFailWithError:(NSError *)error {
    
   
    NSLog(@"%@", error.localizedDescription);
    
    if (self.delegate) {
        
        [self.delegate watchManagerDidFailWithError:error];
    }
}

- (void)client:(NBMRoomClient *)client didJoinRoom:(NSError *)error {
    

    [[UserManager shared].currentUser.userPeers removeAllObjects];
    [[UserManager shared].currentUser.userPeers addObjectsFromArray:client.peers];
    
    if (!error) {
        
        if (self.delegate) {
            
            [self.delegate watchManagerRoomJoined:error];
        }
        
        [self generateLocalOffer];
        
        NSArray *remotePeers = [self.roomClient peers];
        
        for (NBMPeer *peer in remotePeers) {
            NBMPeerConnection *peerConnection = [self connectionOfPeer:peer];
            
            if (!peerConnection && peer.streams.count > 0) {
                
                [self generateOfferForPeer:peer];
            }
        }
        
    } else if (self.delegate) {
        
        [self.delegate watchManagerDidFailWithError:error];
    }
}

- (void)client:(NBMRoomClient *)client didLeaveRoom:(NSError *)error {
    
    [[UserManager shared].currentUser.userPeers removeAllObjects];
    [[UserManager shared].currentUser.userPeers addObjectsFromArray:client.peers];
    //Leaving Room
}

//Room Events

- (void)client:(NBMRoomClient *)client participantJoined:(NBMPeer *)peer {
    
    [[UserManager shared].currentUser.userPeers removeAllObjects];
    [[UserManager shared].currentUser.userPeers addObjectsFromArray:client.peers];
    
    if (self.delegate) {

        [[UserManager shared].currentUser.userPeers addObject:peer];
        
        [self.delegate watchManagerPeerJoined:peer];
    }
}

-(void)client:(NBMRoomClient *)client masterTimeReceived:(NSNumber *)time {
    
    [[UserManager shared].currentUser.userPeers removeAllObjects];
    [[UserManager shared].currentUser.userPeers addObjectsFromArray:client.peers];
    
    [self.delegate watchManagerDidGetStreamTime:time];
    NSLog(@"%@", time);
}

- (void)client:(NBMRoomClient *)client participantLeft:(NBMPeer *)peer {
    
    [[UserManager shared].currentUser.userPeers removeAllObjects];
    [[UserManager shared].currentUser.userPeers addObjectsFromArray:client.peers];
    NSString *connectionId = [self connectionIdOfPeer:peer];
    
    [self.webRTCPeer closeConnectionWithConnectionId:connectionId];
    
    if (self.delegate) {
        
        [[UserManager shared].currentUser.userPeers removeAllObjects];
        [[UserManager shared].currentUser.userPeers addObjectsFromArray:client.peers];
        
        [self.delegate watchManagerPeerLeft:peer];
    }
}

- (void)client:(NBMRoomClient *)client participantEvicted:(NBMPeer *)peer {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(watchManagerPeerEvicted:)]) {
        
        [self.delegate watchManagerPeerEvicted:peer];
    }
}

- (void)client:(NBMRoomClient *)client participantPublished:(NBMPeer *)peer {
    
    [[UserManager shared].currentUser.userPeers removeAllObjects];
    [[UserManager shared].currentUser.userPeers addObjectsFromArray:client.peers];
    
    NBMPeerConnection *peerConnection = [self connectionOfPeer:peer];
    
    if (!peerConnection && peer.streams.count > 0) {
        
        [self generateOfferForPeer:peer];
    }
}

- (void)client:(NBMRoomClient *)client participantUnpublished:(NBMPeer *)peer {
    
    NSString *connectionId = [self connectionIdOfPeer:peer];
    
    [self.webRTCPeer closeConnectionWithConnectionId:connectionId];
}

- (void)client:(NBMRoomClient *)client didReceiveICECandidate:(RTCIceCandidate *)candidate fromParticipant:(NBMPeer *)peer {
    
    NSString *connectionId =[self connectionIdOfPeer:peer];
    
    [self.webRTCPeer addICECandidate:candidate connectionId:connectionId];
}

- (void)client:(NBMRoomClient *)client didReceiveMessage:(NSString *)message fromParticipant:(NBMPeer *)peer {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(watchManagerMessageReceived:ofPeer:)]) {
        
        [self.delegate watchManagerMessageReceived:message ofPeer:peer];
    }
}

- (void)client:(NBMRoomClient *)client mediaErrorOccurred:(NSError *)error {
    
    //Handle this case ?
}

- (void)client:(NBMRoomClient *)client roomWasClosed:(NBMRoom *)room {
    
    //Handle this case ?
}


#pragma mark - Helpers -

- (void)setupRoomClient:(NBMRoom *)room {
    
    self.roomClient = [[NBMRoomClient alloc] initWithRoom:room delegate:self];
}

- (void)manageRoomClientConnection {
    
    BOOL retryAllowed = self.retryCount < 3;
    
    if (retryAllowed && self.roomClient.peers.count < 4) {
        
        self.retryCount++;
        [self.roomClient connect];
        
    } else if (!retryAllowed && self.delegate) {
        
        NSError *retryError = [NSError errorWithDomain:@"it.nubomedia.NBMRoomManager"
                                                  code:0
                                              userInfo:@{NSLocalizedDescriptionKey: @"Impossible to establish WebSocket connection to Room Server, check internet connection"}];
        
        [self.delegate watchManagerDidFailWithError:retryError];
    }
}

- (void)retryRoomClientConnect {
    
    self.retryCount++;
    
    [self.roomClient connect];
}

- (void)setupWebRTCSession {
    
    NBMMediaConfiguration *configurations = [NBMMediaConfiguration defaultConfiguration];
    //    configurations.videoCodec = NBMVideoCodecH264;
    //    configurations.videoBandwidth = 2048;
    //
    //    struct NBMVideoFormat format;
    //
    //    format.frameRate = 30;
    //    format.pixelFormat = NBMPixelFormat420f;
    //    configurations.receiverVideoFormat = format;
    
    NBMWebRTCPeer *webRTCManager = [[NBMWebRTCPeer alloc] initWithDelegate:self configuration:configurations];
    
    if (!webRTCManager && self.delegate) {
        
        NSError *retryError = [NSError errorWithDomain:@"it.nubomedia.NBMRoomManager"
                                                  code:0
                                              userInfo:@{NSLocalizedDescriptionKey: @"Impossible to setup local media stream, check AUDIO & VIDEO permission"}];
        
        [self.delegate watchManagerDidFailWithError:retryError];
        
        return;
    }
    
    self.webRTCPeer = webRTCManager;
    
    BOOL started = [self.webRTCPeer startLocalMedia];
    
    if (started) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (self.delegate) {
                
                [self.delegate watchManagerDidAddLocalStream:self.localStream];
            }
        });
    }
}

- (void)teardownRoomClient {
    
    self.roomClient = nil;
    self.retryCount = 0;
}

- (void)teardownWebRTCSession {
    
    for (NSString *peerID in self.allPeers) {
        
        [self.webRTCPeer closeConnectionWithConnectionId:peerID];
    }
    
    [self.webRTCPeer stopLocalMedia];
    
    [self.mutableRemoteStreams removeAllObjects];
}

- (void)joinToRoom {

    [UserManager shared].currentUser.currentRoom = [UserManager shared].currentUser.roomToConnectTo;
    
        [self.roomClient joinRoomWithUsername:[UserManager shared].currentUser.fullname userEmail:[UserManager shared].currentUser.numberhash networkType:[Utils connectedNetworkType] userToken:@"blahblah" dataChannels:NO];
    
//    [self.roomClient joinRoomWithDataChannels:false];
}

-(void)checkTimer:(NSNumber*)time {
    
    [self.roomClient checkTime:nil hlsTime:time];
}


- (void)generateLocalOffer {
    
    [self generateOfferForPeer:[self localPeer]];
}

- (void)generateOfferForPeer:(NBMPeer *)peer {
    
    NSString *connectionId = [self connectionIdOfPeer:peer];
    
    [self.webRTCPeer generateOffer:connectionId withDataChannels:self.roomClient.room.dataChannels];
}

- (void)safeICERestartForRemotePeer:(NBMPeer *)remotePeer {
    
    [self.roomClient unsubscribeVideoFromPeer:remotePeer completion:^(NSString *sdpAnswer, NSError *error) {
        
        [self generateOfferForPeer:remotePeer];
    }];
}

- (void)safeICERestartForLocalPeer {
    
//    [self.roomClient unpublishVideo:^(NSError *error) {
//
        [self generateLocalOffer];
//    }];
    
}

- (void)safeRestore {
    
    [self.roomClient leaveRoom:^(NSError *error) {
        
        [self joinToRoom];
    }];
}

- (void)restoreConnections {
    
    BOOL restoreLocalPeer = [self peerIsRestorable:[self localPeer]];
    
    if (restoreLocalPeer) {
        
        [self safeICERestartForLocalPeer];
    }
    
    NSArray *remotePeers = [self.roomClient peers];
    
    for (NBMPeer *peer in remotePeers) {
        
        BOOL restoreRemotePeer = [self peerIsRestorable:peer];
        
        if (restoreRemotePeer) {
            
            [self safeICERestartForRemotePeer:peer];
        }
    }
}

- (BOOL)peerIsRestorable:(NBMPeer *)peer {
    
    NBMPeerConnection *localPeerConnection = [self connectionOfPeer:peer];
    
    BOOL isInactive = ![self isActiveConnection:localPeerConnection];
    BOOL hasPublishedStream = peer.streams.count > 0;
    
    return isInactive & hasPublishedStream;
}

- (BOOL)peerHasPublishedMedia:(NBMPeer *)peer {
    
    return peer.streams.count > 0 ? YES : NO;
}

- (void)didSessionRouteChange:(NSNotification *)notification {
    
    AVAudioSessionRouteDescription* route = [[AVAudioSession sharedInstance] currentRoute];
    
    for (AVAudioSessionPortDescription* desc in [route outputs]) {
        
        if ([[desc portType] isEqualToString:AVAudioSessionPortBuiltInReceiver]) {
            
            NSDictionary *interuptionDict = notification.userInfo;
            
            NSInteger routeChangeReason = [[interuptionDict valueForKey:AVAudioSessionRouteChangeReasonKey] integerValue];
            
            switch (routeChangeReason) {
                case AVAudioSessionRouteChangeReasonOldDeviceUnavailable: {
                    
                    NSError* error;
                    [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
                    
                    break;
                }
                case AVAudioSessionRouteChangeReasonCategoryChange: {
                    
                    NSError* error;
                    [[AVAudioSession sharedInstance] overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:&error];
                    
                    break;
                }
                    
                default:
                    break;
            }
        }
    }
}

#pragma mark - Peers & Connections

- (NSSet *)allPeers {
    
    NSMutableSet *allPeers = [NSMutableSet setWithArray:self.remotePeers];
    
    if ([self localPeer]) {
        
        [allPeers addObject:[self localPeer]];
    }
    
    return [allPeers copy];
}

- (NSString *)localConnectionId {
    
    NBMPeer *localPeer = [self localPeer];
    
    return [self connectionIdOfPeer:localPeer];
}

- (NBMPeer *)peerOfConnection:(NBMPeerConnection *)connection {
    
    NSString *connectionId = connection.connectionId;
    
    __block NBMPeer *peer;
    
    [self.allPeers enumerateObjectsUsingBlock:^(NBMPeer *aPeer, BOOL  *stop) {
        
        NSString *connectionIdOfPeer = [self connectionIdOfPeer:aPeer];
        
        if ([connectionIdOfPeer isEqualToString:connectionId]) {
            
            peer = aPeer;
            *stop = YES;
        }
    }];
    
    return peer;
}

- (NBMPeerConnection *)connectionOfPeer:(NBMPeer *)peer {
    
    NSString *connectionId = [self connectionIdOfPeer:peer];
    
    NBMPeerConnection *connection = [self.webRTCPeer connectionWithConnectionId:connectionId];
    
    return connection;
}

- (NSString *)connectionIdOfPeer:(NBMPeer *)peer {
    
    if (!peer) {
        
        peer = [self localPeer];
    }
    
    NSString *connectionId = peer.identifier;
    
    return connectionId;
}

- (BOOL)isActiveConnection:(NBMPeerConnection *)connection {
    
    RTCPeerConnection *rtcConnection = connection.peerConnection;
    
    if (rtcConnection.signalingState == RTCSignalingStateStable && rtcConnection.iceConnectionState != RTCIceConnectionStateFailed) {
        
        return YES;
    }
    
    return NO;
}

@end
