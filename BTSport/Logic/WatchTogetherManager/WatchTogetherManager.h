//
//  WatchTogetherManager.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import <Foundation/Foundation.h>

#import "User.h"

#import <KurentoToolbox.h>

@protocol WatchTogetherManagerDelegate <NSObject>

- (void)watchManagerDidFinish;

- (void)watchManagerDidAddLocalStream:(RTCMediaStream *)localStream;

- (void)watchManagerDidRemoveLocalStream:(RTCMediaStream *)localStream;

- (void)watchManagerPeerJoined:(NBMPeer *)peer;

- (void)watchManagerPeerLeft:(NBMPeer *)peer;

- (void)watchManagerRoomJoined:(NSError *)error;

- (void)watchManagerDidFailWithError:(NSError *)error;

- (void)watchManagerDidGetStreamTime:(NSNumber*)time;

@optional

- (void)watchManagerDidAddStream:(RTCMediaStream *)remoteStream ofPeer:(NBMPeer *)remotePeer;

- (void)watchManagerDidRemoveStream:(RTCMediaStream *)remoteStream ofPeer:(NBMPeer *)remotePeer;

- (void)watchManagerPeerEvicted:(NBMPeer *)peer;

- (void)watchManagerMessageReceived:(NSString *)message ofPeer:(NBMPeer *)peer;

- (void)watchManagerPeerStatusChanged;

- (void)watchManagerIceStatusChanged:(RTCIceConnectionState)state ofPeer:(NBMPeer *)peer;

- (void)watchManagerDidAddDataChannel:(RTCDataChannel *)dataChannel;

@end

@interface WatchTogetherManager : NSObject

@property (nonatomic,weak) id<WatchTogetherManagerDelegate> delegate;

@property (nonatomic, strong, readonly) RTCMediaStream *localStream;
@property (nonatomic, strong, readonly) NBMPeer *localPeer;

@property (nonatomic, strong, readonly) NSArray *remotePeers;
@property (nonatomic, strong, readonly) NSArray *remoteStreams;

@property (nonatomic, assign, readonly) NBMCameraPosition cameraPosition;

@property (nonatomic, assign, readonly, getter=isConnected) BOOL connected;
@property (nonatomic, assign, readonly, getter=isJoined) BOOL joined;

@property (nonatomic) BOOL shouldHandleNotification;

- (instancetype)initWithDelegate:(id)delegate;

- (void)startLocalVideo;

- (void)stopLocalVideo;

- (void)startWatching;

- (void)stopWatching:(void (^)(NSError *))block;

- (void)publishVideo:(void (^)(NSError *error))block loopback:(BOOL)doLoopback;

- (void)unpublishVideo:(void (^)(NSError *error))block;

- (void)receiveVideoFromPeer:(NBMPeer *)peer completion:(void (^)(NSError *error))block;

- (void)unsubscribeVideoFromPeer:(NBMPeer *)peer completion:(void (^)(NSError *error))block;

- (void)toggleAudioFromPeer:(NBMPeer *)peer completion:(void (^)(BOOL enabled))block;

- (void)receiveAudioFromPeer:(NBMPeer *)peer completion:(void (^)(BOOL success))block;

- (void)unsubscribeAudioFromPeer:(NBMPeer *)peer completion:(void (^)(BOOL success))block;

- (void)selectCameraPosition:(NBMCameraPosition)cameraPosition;

- (BOOL)isVideoEnabled;

- (void)enableVideo:(BOOL)enable;

- (BOOL)isAudioEnabled;

- (void)enableAudio:(BOOL)enable;

-(void)checkTimer:(NSNumber*)time ;

- (NBMPeer*)remotePeerWithIdentifier:(NSString*)identifier;

@end
