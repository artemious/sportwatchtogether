//
//  AppDelegate.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//

#import "AppDelegate.h"

#import <UserNotifications/UserNotifications.h>
#import <AVFoundation/AVFoundation.h>
#import "WatchTogetherManager.h"
#import "InvitesManager.h"
#import "UserManager.h"
#import "ApiManager.h"
#import "SplashController.h"
#import "LoginController.h"
#import "StreamsListController.h"
#import "StreamDetailsController.h"
#import "StreamWatchController.h"
#import "MainTabViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "Firebase.h"
#import "ContactsManager.h"
#import "Branch/Branch.h"
#import <FirebaseMessaging/FirebaseMessaging.h>
#import "Utils.h"
#import "HideScreenViewController.h"

@interface AppDelegate () <FIRMessagingDelegate, UNUserNotificationCenterDelegate> {
    
    NSString *handledPushID;
    HideScreenViewController* hideVC;
}

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    hideVC = [HideScreenViewController new];
    
    hideVC.view.frame = self.window.bounds;
    
    
    if([Utils isJailBroken]) {
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentUser"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        UINavigationController *rootController = (UINavigationController*)self.window.rootViewController;
        
        if ([rootController.viewControllers.lastObject isKindOfClass:[MainTabViewController class]]) {
            
            MainTabViewController* tabBAr =  (MainTabViewController*)rootController.viewControllers.lastObject;
            
            if ([tabBAr.selectedViewController isKindOfClass:[StreamsListController class]]) {
                
                StreamsListController *streamsListController = (StreamsListController*)tabBAr.selectedViewController;
                
                [streamsListController.navigationController popViewControllerAnimated:YES];
            } else {
                
                [tabBAr setSelectedIndex:1];
                
                StreamsListController *streamsListController = (StreamsListController*)tabBAr.selectedViewController;
                
                [streamsListController.navigationController popViewControllerAnimated:YES];
            }
        }
        
        if ([rootController.presentedViewController isKindOfClass:[StreamDetailsController class]]) {
            
            StreamDetailsController *streamDetailsController = (StreamDetailsController*)self.window.rootViewController.presentedViewController ;
            
            [streamDetailsController.navigationController popViewControllerAnimated:YES];
            
        } else if ([self.window.rootViewController  isKindOfClass:[StreamWatchController class]]) {
            
            StreamWatchController *streamWatchController = (StreamWatchController*)self.window.rootViewController.presentedViewController;
            
            [streamWatchController.navigationController popViewControllerAnimated:YES];
        }
    }
        
    
    [[Branch getInstance] setDebug];
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayAndRecord error:nil];

    [Fabric with:@[[Crashlytics class]]];
    
    [self configureFirebselWithLaunchOption];
    
    [[Branch getInstance] initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary * _Nonnull params, NSError * _Nullable error) {
        NSLog(@"%@", params);
    }];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
  
    [hideVC.view removeFromSuperview];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
   
    [self.window addSubview:hideVC.view];
}



- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [hideVC.view removeFromSuperview];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    
    hideVC = nil;
    

}

- (void)applicationWillEnterForeground:(UIApplication *)application {

    [self.window addSubview:hideVC.view];
    
    [ContactsManager getContacts:^{
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"AppDidEnterForeground"
         object:self];
    }];
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    [[Branch getInstance] application:app openURL:url options:options];
    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    // pass the url to the handle deep link call
//    [[Branch getInstance] handleDeepLink:url];
    
    // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
    return YES;
}

- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler {
    [[Branch getInstance] continueUserActivity:userActivity];
    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(nonnull NSData *)deviceToken {
    
    [[FIRMessaging messaging] setAPNSToken:deviceToken];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    [self handleNotificationOrDeepLinkData:userInfo];
    
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    [[Branch getInstance] handlePushNotification:userInfo];
}

#pragma mark - Deep Link and Notifications Data Handling

- (void)handleNotificationOrDeepLinkData:(NSDictionary*)data {
    
    UINavigationController *rootController = (UINavigationController*)self.window.rootViewController;
    Invite *newInvite = [InviteBuilder buildInviteFromParameters:data];
    
    if (newInvite) {
        
        [[InvitesManager shared] addNewInvite:newInvite];
        
         if ([rootController.viewControllers.lastObject isKindOfClass:[MainTabViewController class]]) {
             
             MainTabViewController* tabBAr =  (MainTabViewController*)rootController.viewControllers.lastObject;
             
             if ([tabBAr.selectedViewController isKindOfClass:[StreamsListController class]]) {
                 
                 StreamsListController *streamsListController = (StreamsListController*)tabBAr.selectedViewController;
                 
                 [streamsListController handleNotificationIfNeeded];
             } else {
                 
                [tabBAr setSelectedIndex:1];
                 
                StreamsListController *streamsListController = (StreamsListController*)tabBAr.selectedViewController;
                 
                [streamsListController handleNotificationIfNeeded];
             }
         }
        
        if ([rootController.presentedViewController isKindOfClass:[StreamDetailsController class]]) {
            
            StreamDetailsController *streamDetailsController = (StreamDetailsController*)self.window.rootViewController.presentedViewController ;
            
            [streamDetailsController handleNotificationIfNeeded];
            
        } else if ([self.window.rootViewController  isKindOfClass:[StreamWatchController class]]) {
            
            StreamWatchController *streamWatchController = (StreamWatchController*)self.window.rootViewController.presentedViewController;
            
            [streamWatchController handleNotificationIfNeeded];
        }
    }
}

- (void)configureFirebselWithLaunchOption {
    
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0f){
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
         }];
    } else {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName: @"FCMToken" object:nil userInfo:dataDict];
}

- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
    
    if (fcmToken && ![fcmToken isEqualToString:[UserManager shared].currentUser.pushID]) {
        
        [UserManager shared].currentUser.pushID = fcmToken;
        
        [[ApiManager shared] registerPushToken:fcmToken completionBlock:nil];
    }
}

@end
