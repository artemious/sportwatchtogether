//
//  ApiManager.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CompletionBlock)(id responce, NSError *error);

@interface ApiManager : NSObject

+ (instancetype)shared;

#pragma mark - Common

- (void)loginWithEmail:(NSString*)email completionBlock:(CompletionBlock)completion;

- (void)signupWithFirstname:(NSString*)firstname lastname:(NSString*)lastname email:(NSString*)email completionBlock:(CompletionBlock)completion;

- (void)getDeviceDescriptionForLocation:(NSString*)location completionBlock:(CompletionBlock)completion;

- (void)updateAppVersion;

-(void)deviceIsJailborkenAPI;

#pragma mark - History

- (void)getRoomUsers:(NSString*)roomID completionBlock:(CompletionBlock)completion;
- (void)getRoomUsers:(CompletionBlock)completion;
-(void) inviteFriends:(NSArray*)friends completionBlock:(CompletionBlock)completion;

#pragma mark - Notifications

- (void)registerPushToken:(NSString*)pushToken completionBlock:(CompletionBlock)completion;

#pragma mark - Analytics

- (void)logOpenLinkAction:(NSString*)link completionBlock:(CompletionBlock)completion;

#pragma mark - YouTube Channel Stream List

- (void)getStreamsList:(NSString*)urlString andCompletion:(CompletionBlock)completionBlock;

#pragma mark - Firebase Post Notification

-(void)sendNotificationForGuestConversation:(NSDictionary*)guestParameters andCompletion: (void (^)(BOOL success))block;
@end
