//
//  ApiManager.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//

#import "ApiManager.h"

#import "UserManager.h"

#import "User.h"

#import <AFNetworking.h>

#import "XMLDictionary.h"

#import "Firebase.h"

#import <CommonCrypto/CommonDigest.h>

#import "Utils.h"

#import "Contact.h"

//static NSString *kBaseURL = @"http://192.168.2.202:5000/";
//static NSString *kBaseURL = @"https://btsport.sceenic.co/";
static NSString *kBaseURL = @"https://sportwt.sceenic.co/";
static NSString *kJaibroken = @"api/user/root_detected";
static NSString *kLoginURL = @"api/user/login";
static NSString *kUpdateVersionURL =@"api/user/update_client_version";
static NSString *kGetFriendsUsers = @"/get_friends";

static NSString *kRegisterPushTokenURL = @"api/user/update_push_id";

static NSString *kGetRoomUsers = @"api/room?filter_key=roomid&filter_val=%@&isForMobile=true";

static NSString *kLogSendInvitationLinkActionURL = @"api/users/usersendinvitation";
static NSString *kLogSendInvitationTofriend = @"api/user/invite";
static NSString *kLogLinkActionURL = @"api/users/useropeninvitation";

typedef enum : NSUInteger {
    Open,
    Join
} LinkActionType;

@interface ApiManager () {
    
    User *currentUser;
}

@end

@implementation ApiManager

+ (instancetype)shared {
    
    static ApiManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        sharedInstance = [[ApiManager alloc] init];
    });
    
    if ([Utils isJailBroken]) {
        
        return nil;
    } else {
        
        return sharedInstance;
    }
}

#pragma mark - Common

- (void)loginWithEmail:(NSString*)email completionBlock:(CompletionBlock)completion {
    
    NSString* hash = [self sha256HashFor:email];
    
    NSDictionary *parameters = @{@"phone_hash" : hash,
                                 @"push_id" :  @"push_id_8c49eccebdd22ca61c1dd87f34e6b7854547984eecd402d5e0bb76e8b98d35f6",
                                 @"device_id_hash" :  [[UIDevice currentDevice] identifierForVendor].UUIDString};
    
    [self post:[kBaseURL stringByAppendingString:kLoginURL] parameters:parameters tokenNeeded:NO completionBlock:completion];
}

- (void)updateAppVersion {
    
    NSString* appVersion = @"1.0.2";
    NSString* deviceId = [self getUUID];
    NSString* hash = [UserManager shared].currentUser.numberhash;
    
    if (![appVersion isEqualToString:[UserManager shared].currentUser.appVersion]) {
        
        [UserManager shared].currentUser.appVersion = appVersion;
        
        NSDictionary *parameters = @{
                                     @"device_id_hash" :  deviceId,
                                     @"phone_hash" : hash,
                                     @"token" : [UserManager shared].currentUser.token,
                                     @"version" : appVersion
                                     };
        
        [self post:[kBaseURL stringByAppendingString:kUpdateVersionURL] parameters:parameters tokenNeeded:NO completionBlock:nil];
    }
}

- (void)signupWithFirstname:(NSString*)firstname lastname:(NSString*)lastname email:(NSString*)email completionBlock:(CompletionBlock)completion {
    
    NSString* fcmToken = [FIRMessaging messaging].FCMToken;
    NSString* deviceId = [self getUUID];
    NSString* hash = [self sha256HashFor:email];
    
    currentUser = [User initWithData:email hashNumber:hash deviceName: [[UIDevice currentDevice] name] firstName:firstname lastName:lastname  deviceId:deviceId pushId:fcmToken];

    [currentUser save];
    
    [UserManager shared].currentUser = currentUser;
    
    [UserManager shared].currentUser.appVersion = @"1.0.2";
    
    if ([Utils isJailBroken]) {
        
        [self deviceIsJailborkenAPI];
    }
    
    NSDictionary *parameters = @{
                                 @"device_os" : [[UIDevice currentDevice] systemVersion],
                                 @"device_name" : [[UIDevice currentDevice] name],
                                 @"phone_hash" : hash,
                                 @"app_version" : [UserManager shared].currentUser.appVersion,
                                 @"push_id" :  fcmToken,
                                 @"device_id_hash" :  deviceId
                                 };
    

    [self post:[kBaseURL stringByAppendingString:kLoginURL] parameters:parameters tokenNeeded:NO completionBlock:completion];
}

- (void)getDeviceDescriptionForLocation:(NSString*)location completionBlock:(CompletionBlock)completion {
    
    [self get:location parameters:nil tokenNeeded:NO completionBlock:completion];
}

#pragma mark - History

- (void)getRoomUsers:(NSString*)roomID completionBlock:(CompletionBlock)completion {
    
    [self get:[NSString stringWithFormat:[kBaseURL stringByAppendingString:kGetRoomUsers], roomID] parameters:nil tokenNeeded:YES completionBlock:completion];
}

- (void)getRoomUsers:(CompletionBlock)completion {
    
    NSString* url = [NSString stringWithFormat:@"%@api/user%@",kBaseURL, kGetFriendsUsers];
    NSMutableArray* friedsHashes = [NSMutableArray new];
    
    for (Contact* phoneBookContact in [UserManager shared].currentUser.userPhoneContacts) {
        
        [friedsHashes addObject:phoneBookContact.numberHash];
    }
    
    NSDictionary *parameters = @{@"token" : [UserManager shared].currentUser.token,
                                 @"phone_hash" : [UserManager shared].currentUser.numberhash,
                                 @"device_id_hash" :  [UserManager shared].currentUser.deviceid,
                                 @"phone_hash_list" : friedsHashes
                                 };
    
    [self post:url parameters:parameters tokenNeeded:YES completionBlock:completion];
}

-(void)inviteFriends:(NSArray*)friends completionBlock:(CompletionBlock)completion {
    
    NSString* deviceId = [self getUUID];
    
    NSDictionary *parameters = @{@"invite" :  friends,
                                 @"token" : [UserManager shared].currentUser.token,
                                 @"phone_hash" : [UserManager shared].currentUser.numberhash,
                                 @"device_id_hash" :  [UserManager shared].currentUser.deviceid,
                                 @"device_id_hash" :  deviceId};
    
    [self post:[kBaseURL stringByAppendingString:kLogSendInvitationTofriend] parameters:parameters tokenNeeded:NO completionBlock:completion];
}

-(void)deviceIsJailborkenAPI {
    
    NSDictionary *parameters = @{
                                 @"device_id_hash" : [UserManager shared].currentUser.deviceid
                                 };
    
    [self post:[kBaseURL stringByAppendingString:kJaibroken] parameters:parameters tokenNeeded:NO completionBlock:nil];
}

#pragma mark - Notifications

- (void)registerPushToken:(NSString*)pushToken completionBlock:(CompletionBlock)completion {
    
    if ([UserManager shared].currentUser.devicename) {
        
        NSDictionary *parameters = @{@"device_id_hash" : [UserManager shared].currentUser.deviceid,
                                     @"token" : [UserManager shared].currentUser.token,
                                     @"phone_hash" : [UserManager shared].currentUser.numberhash,
                                     @"push_id" : pushToken};
        
        [self post:[kBaseURL stringByAppendingString:kRegisterPushTokenURL] parameters:parameters tokenNeeded:YES completionBlock:completion];
    }
}

#pragma mark - Analytics

- (void)logOpenLinkAction:(NSString*)link completionBlock:(CompletionBlock)completion {
    
    [self logActionOnAnInvitationLink:link actionType:Open completionBlock:completion];
}

#pragma mark - Private

- (void)logActionOnAnInvitationLink:(NSString*)link actionType:(LinkActionType)actionType completionBlock:(CompletionBlock)completion {
    
    NSMutableDictionary *parameters = [@{@"InvitedUserEmail" : [UserManager shared].currentUser.number,
                                         @"BranchLink" : link} mutableCopy];
    
    if (actionType == Join) {
        
        parameters[@"IsJoined"] = @"true";
    }
    
    [self post:[kBaseURL stringByAppendingString:kLogLinkActionURL] parameters:parameters tokenNeeded:YES completionBlock:completion];
}

- (void)get:(NSString*)URL parameters:(NSDictionary*)parameters tokenNeeded:(BOOL)isTokenNeeded completionBlock:(CompletionBlock)completion {
    
    AFHTTPSessionManager *networkManager = [self getNetworkManager];
    
//    if (isTokenNeeded) {
//        
//        [networkManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer  %@", [UserManager shared].currentUser.token] forHTTPHeaderField:@"Authorization"];
//    }
    
    [networkManager GET:URL parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self serializeResponse:responseObject task:task completion:^(id serializedResponse, NSError *error) {
            
            if (!error) {
                
                completion(serializedResponse, nil);
                
            } else {
                
                completion(nil, error);
            }
            
        }];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        completion(nil, error);
    }];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void)post:(NSString*)URL parameters:(NSDictionary*)parameters tokenNeeded:(BOOL)isTokenNeeded completionBlock:(CompletionBlock)completion {
    
    AFHTTPSessionManager *networkManager = [self getNetworkManager];
    
//    if (isTokenNeeded) {
//
//        [networkManager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer  %@", [UserManager shared].currentUser.token] forHTTPHeaderField:@"Authorization"];
//    }
    
    [networkManager POST:URL parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self serializeResponse:responseObject task:task completion:^(id serializedResponse, NSError *error) {
            
            if (completion) {
                
                if (!error) {
                    
                    completion(serializedResponse, nil);
                    
                } else {
                    
                    completion(nil, error);
                }
            }
        }];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if (completion) {
            
            completion(nil, error);
        }
    }];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

#pragma mark - Helpers

-(NSString*)sha256HashFor:(NSString*)input {
    
    const char* str = [input UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
    {
        [ret appendFormat:@"%02x",result[i]];
    }

    return ret;
}

-(NSString *) md5:(NSString *) input {
    
    const char *cStr = [input UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (uint32_t)strlen(cStr), digest);
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]]; //%02X for capital letters
    return output;
}

-(NSString*) getUUID {
    
    NSString* deviceId = [self md5:[self sha256HashFor:[[UIDevice currentDevice] identifierForVendor].UUIDString]];
    NSString* newDeviceId = [NSString stringWithFormat:@"%@-%@-%@-%@-%@",[deviceId substringWithRange:NSMakeRange(0, 8)],[deviceId substringWithRange:NSMakeRange(9, 4)], [deviceId substringWithRange:NSMakeRange(14, 4)], [deviceId substringWithRange:NSMakeRange(18, 4)], [deviceId substringWithRange:NSMakeRange(23, 9)]];

    return newDeviceId;
}

- (void)serializeResponse:(id _Nullable)responseObject task:(NSURLSessionDataTask*)task completion:(void (^)(id serializedResponse, NSError *error))comletionBlock {
    
    if ([responseObject isKindOfClass:[NSData class]]) {
        
        NSData *data = responseObject;
        
        if (data.length > 0)
        {
            NSString *responseType = ((NSHTTPURLResponse*)task.response).allHeaderFields[@"Content-Type"];
            
            if ([responseType rangeOfString:@"xml"].location != NSNotFound) {
                
                NSMutableDictionary *deviceDescription = [[NSDictionary dictionaryWithXMLData:responseObject] mutableCopy];
                
                if (((NSHTTPURLResponse*)task.response).allHeaderFields[@"Application-URL"]) {
                    
                    [deviceDescription setObject:((NSHTTPURLResponse*)task.response).allHeaderFields[@"Application-URL"] forKey:@"Application-URL"];
                }
                
                comletionBlock(deviceDescription, nil);
                
            } else {
                
                NSError* serializationError = nil;
                
                id responseObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&serializationError];
                
                if (serializationError)
                {
                    if (comletionBlock)
                    {
                        comletionBlock(nil, serializationError);
                    }
                } else {
                    
                    if (comletionBlock)
                    {
                        comletionBlock(responseObject, nil);
                    }
                }
            }
            
        } else {
            
            NSError *dataLenghtError = [[NSError alloc] initWithDomain:@"35.166.91.188" code:0 userInfo:@{NSLocalizedDescriptionKey : @"Server error occured. Please try again later."}];
            
            comletionBlock(nil, dataLenghtError);
        }
        
    } else {
        
        NSError *emptyDataError = [[NSError alloc] initWithDomain:@"35.166.91.188" code:0 userInfo:@{NSLocalizedDescriptionKey : @"Server error occured. Please try again later."}];
        
        comletionBlock(nil, emptyDataError);
    }
}

- (AFHTTPSessionManager*)getNetworkManager {
    
    AFHTTPSessionManager *networkManager = [AFHTTPSessionManager manager];
    
    AFHTTPResponseSerializer * responseSerializer = [AFHTTPResponseSerializer serializer];
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/xml", @"application/json", nil];
    
    networkManager.responseSerializer = responseSerializer;
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    [requestSerializer setTimeoutInterval:15];
    
    networkManager.requestSerializer = requestSerializer;
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModePublicKey];
    networkManager.securityPolicy = securityPolicy;
    NSString *cerPath = [[NSBundle mainBundle]pathForResource:@"sceenicCert" ofType:@"cer"];
    NSData *certData = [NSData dataWithContentsOfFile:cerPath];
    [networkManager.securityPolicy setPinnedCertificates:[NSSet setWithObject:certData]];

    return networkManager;
}

#pragma mark -  Streams List Request

- (void) getStreamsList:(NSString *)urlString andCompletion:(CompletionBlock)completionBlock{
    
    [self getWithUrlString:urlString tokenNeeded:YES andCompletion:completionBlock];
}

#pragma mark -  Streams List Request Helper

-(void) getWithUrlString:(NSString*)urlString tokenNeeded:(BOOL)isTokenNeeded andCompletion:(CompletionBlock)completionBlock {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSDictionary *parameters = @{@"phone_hash" : [UserManager shared].currentUser.numberhash,
                                 @"device_id_hash" : [UserManager shared].currentUser.deviceid,
                                 @"token" : [UserManager shared].currentUser.token
                                 };
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
//    if (isTokenNeeded) {
//        
//        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer  %@", [UserManager shared].currentUser.token] forHTTPHeaderField:@"Authorization"];
//    }
    
    [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        completionBlock(responseObject, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        completionBlock(nil, error);
        
    }];
    
     [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

#pragma mark - Firebase Post Notification

-(void)sendNotificationForGuestConversation:(NSDictionary*)guestParameters andCompletion: (void (^)(BOOL success))block {
    
    NSString* url = @"https://fcm.googleapis.com/fcm/send";
    
    [self postWithUrlString:url andParams:guestParameters withCompletion:^(BOOL success) {
        
        if(success){
            
            block(success);
        }else {
            
            block(success);
        }
    }];
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

#pragma mark - Firebase Post Notification Helper

-(void) postWithUrlString:(NSString*)urlString andParams:(NSDictionary*)parameters withCompletion:(void (^)(BOOL success))block {
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    AFHTTPResponseSerializer * responseSerializer = [AFHTTPResponseSerializer serializer];
    
    responseSerializer.acceptableContentTypes = [NSSet setWithObjects: @"application/json", nil];
    
    manager.responseSerializer = responseSerializer;

    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"key=AAAAGLG96DA:APA91bH7d6IREJNUAmDKChOtGTLMAaZrL0KXBF_Ukij_tK61sexSM5s-rqih0tU1DNpRDQ90uUDwK2qikkmTn_R1wOCHlnDFn3okFawoetcAdZHOot0bfLthYV-Iulvnxq-9jUvxS-n4" forHTTPHeaderField:@"Authorization"];

    [manager POST:urlString parameters: parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        block(YES);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"%@", error);
        block(NO);
        
    }];
    
     [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

@end
