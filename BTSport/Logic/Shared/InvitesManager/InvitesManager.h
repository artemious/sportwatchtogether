//
//  InvitesManager.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import <Foundation/Foundation.h>

@class Invite, InitialInvite, WatchInvite;

typedef NS_ENUM(NSUInteger, InviteType) {
    Unknown,
    Initial,
    Watch
};

@interface InviteBuilder: NSObject

+ (Invite*)buildInviteFromParameters:(NSDictionary*)params;

@end

@interface InvitesManager : NSObject

+ (instancetype)shared;

- (void)addNewInvite:(Invite*)invite;

- (WatchInvite*)getUnhandledWatchInvite;
- (InitialInvite*)getInitialInvite;

@end

@interface Invite : NSObject

@property (nonatomic) InviteType type;

- (instancetype)initWithParameters:(NSDictionary*)params;

@end

@interface WatchInvite : Invite

@property (nonatomic) BOOL isHandled;

- (NSString*)getRoomID;
- (NSString*)getInvitationMessage;
- (NSString*)getBranchLink;

- (NSString*)getChannelName;
- (NSString*)getShowName;
- (NSString*)getChannelID;
- (NSString*)getVideoID;

@end

@interface InitialInvite : Invite

- (NSString*)getUserFirstname;
- (NSString*)getUserLastname;
- (NSString*)getUserEmail;

@end
