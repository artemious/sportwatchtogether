//
//  InvitesManager.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "InvitesManager.h"

@implementation InviteBuilder

+ (Invite*)buildInviteFromParameters:(NSDictionary*)params {
    
    if (params[@"room"] && params[@"channel_name"] && params[@"stream_url"]) {
        //&& params[@"channel_name"]
        
        return [[WatchInvite alloc] initWithParameters:params];
        
    } else {
        
        return nil;
    }
//    } else if (params[@"user_name"]) {
//
//        return [[InitialInvite alloc] initWithParameters:params];
//
//    } else {
        
}

@end


@interface InvitesManager() {
    
    WatchInvite *_watchInvite;
    InitialInvite *_initialInvite;
}

@end

@implementation InvitesManager

#pragma mark - Initialization

+ (instancetype)shared {
    
    static InvitesManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[InvitesManager alloc] init];
    });
    
    return sharedInstance;
}

#pragma mark - Public

- (void)addNewInvite:(Invite*)invite {
    
    if (invite.type == Watch) {
        
        _watchInvite = (WatchInvite*)invite;
        
    } else {
        
        _initialInvite = (InitialInvite*)invite;
    }
}

- (WatchInvite*)getUnhandledWatchInvite {
    
    if (_watchInvite.isHandled) {
        
        _watchInvite = nil;
    }
    
    return _watchInvite;
}

- (InitialInvite *)getInitialInvite {
    
    return _initialInvite;
}

@end

@implementation Invite

- (instancetype)initWithParameters:(NSDictionary *)params
{
    self = [super init];
    
    return self;
}

@end

@interface WatchInvite() {
    
    NSString *_roomID;
    NSString *_organizerName;
    NSString *_branchLink;
    NSString *_channelName;
    NSString *_channelID;
    NSString *_dateMls;
    NSString *_showName;
    NSString *_videoID;
    NSString *_organizerNameFromBranch;
    
    NSDictionary* _aps;
    NSDictionary* _alert;
}

@end

@implementation WatchInvite

#pragma mark - Initialization

- (instancetype)initWithParameters:(NSDictionary*)params {
    
    self = [super initWithParameters:nil];
    
    if (self) {
        
        self->_roomID = params[@"room"];
        self->_channelName = params[@"channel_name"];
        self->_videoID = params[@"stream_url"];
        self->_dateMls = params[@"date_mls"];
        self->_organizerNameFromBranch = params[@"$og_description"];
        self->_aps = params[@"aps"];
        self->_alert = _aps[@"alert"];
        self->_organizerName  = _alert[@"title"];
        
        self.type = Watch;
    }
    
    return self;
}

#pragma mark - Getters

- (NSString*)getRoomID {
    
    return _roomID;
}



- (NSString*)getChannelName {
    
    return _channelName;
}

- (NSString*)getShowName {
    
    return _showName;
}

- (NSString*)getChannelID {
    
    return _channelID;
}

- (NSString*)getVideoID {
    
    return _videoID;
}

#pragma mark - Public

- (NSString*)getInvitationMessage {
    
    NSString* string = nil;
    
    if ([_organizerName length] == 0) {
        string = [NSString stringWithFormat:@"%@", _organizerNameFromBranch];
    } else {
        string = [NSString stringWithFormat:@"%@ has invited you to watch together %@", _organizerName, _channelName];
    }
    
    return string;
}

@end

@interface InitialInvite(){
    
    NSString *_firstName;
    NSString *_lastName;
    NSString *_userEmail;
}

@end

@implementation InitialInvite

#pragma mark - Initialization

- (instancetype)initWithParameters:(NSDictionary*)params {
    
    self = [super initWithParameters:nil];
    
    if (self) {
        
        self->_firstName = params[@"user_name"];
        
        self.type = Initial;
    }
    
    return self;
}

#pragma mark - Getters

- (NSString*)getUserFirstname {
    
    return _firstName;
}

- (NSString*)getUserLastname {
    
    return _lastName;
}

- (NSString*)getUserEmail {
    
    return _userEmail;
}

@end
