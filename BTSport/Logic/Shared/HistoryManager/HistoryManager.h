//
//  HistoryManager.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface HistoryManager : NSObject

+ (instancetype)shared;

- (NSDictionary*)getHistoryList;

- (void)updateHistoryList;

@end

@interface HistoryUser : NSObject <NSCoding>

@property (nonatomic, strong) NSString *firstname;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSArray *pushIDs;
@property (nonatomic, strong) NSDate *lastSeenDate;

- (instancetype)initWithData:(NSDictionary*)userData;

@end
