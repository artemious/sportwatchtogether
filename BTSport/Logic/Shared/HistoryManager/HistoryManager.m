//
//  HistoryManager.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "HistoryManager.h"
#import "ApiManager.h"
#import "UserManager.h"

@interface HistoryManager () {
    
    NSDictionary *_historyList;
}

@end

@implementation HistoryManager

#pragma mark - Initialization

+ (instancetype)shared {
    
    static HistoryManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[HistoryManager alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        
        NSDictionary *historyList = [self getSavedHistoryList];
        
        _historyList = historyList ? historyList : [[NSDictionary alloc] init];
    }
    
    return self;
}

#pragma mark - Public

- (NSDictionary*)getHistoryList {
    
    return _historyList;
}

- (void)updateHistoryList {
    
    if ([UserManager shared].currentUser.currentRoom) {
        
        [[ApiManager shared]getRoomUsers:^(id responce, NSError *error) {
            
            if ([responce isKindOfClass:[NSDictionary class]] && [responce[@"Rooms"] isKindOfClass:[NSArray class]]) {
                
                if (((NSArray*)responce[@"Rooms"]).firstObject[@"Organizer"]) {
                    
                    [self addUserToHistory:((NSArray*)responce[@"Rooms"]).firstObject[@"Organizer"]];
                }
                
                for (NSDictionary *viewerData in ((NSArray*)responce[@"Rooms"]).firstObject[@"Viewers"]) {
                    
                    [self addUserToHistory:viewerData];
                }
            }
        }];
    }
}

#pragma mark - Private

- (void)addUserToHistory:(NSDictionary*)userData {
    
    if (userData[@"Email"] && !([userData[@"Email"] isEqualToString:[UserManager shared].currentUser.number]) && userData[@"FirstName"] && userData[@"LastName"] && userData[@"MobilePushIds"]) {
        
        HistoryUser *historyUser = [[HistoryUser alloc] initWithData:userData];
        historyUser.lastSeenDate = [NSDate date];
        NSMutableDictionary *updatedHistoryList = [_historyList mutableCopy];
        
        for (NSString *dateString in _historyList.allKeys) {
            
            for (HistoryUser *alreadySavedUser in _historyList[dateString]) {
                
                if ([historyUser.number isEqualToString:alreadySavedUser.number]) {
                    
                    NSMutableArray *updatedArr = [_historyList[dateString] mutableCopy];
                    
                    [updatedArr removeObject:alreadySavedUser];
                    
                    if (updatedArr.count == 0) {
                        
                        [updatedHistoryList removeObjectForKey:dateString];
                        
                    } else {
                        
                        updatedHistoryList[dateString] = updatedArr;
                    }
                    
                    break;
                }
            }
        }
        
        NSDateFormatter *dateformatter = [[NSDateFormatter alloc] init];
        
        dateformatter.dateStyle = NSDateFormatterShortStyle;
        
        NSArray *usersForDate = updatedHistoryList[[dateformatter stringFromDate:[NSDate date]]];
        
        NSArray *updatedUsersForDate = @[historyUser];
        
        if (usersForDate) {
            
            updatedUsersForDate = [updatedUsersForDate arrayByAddingObjectsFromArray:usersForDate];
        }
        
        [updatedHistoryList setObject:updatedUsersForDate forKey:[dateformatter stringFromDate:[NSDate date]]];
        
        _historyList = updatedHistoryList;
        
        [self save];
    }
}

#pragma mark - Archiving and Unarchiving

- (void)save {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:_historyList];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"historyList"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSDictionary*)getSavedHistoryList {
    
    NSData *historyData = [[NSUserDefaults standardUserDefaults] objectForKey:@"historyList"];
    
    NSDictionary *historyList = [NSKeyedUnarchiver unarchiveObjectWithData:historyData];
    
    return historyList;
}

@end

#pragma mark - History User -

@implementation HistoryUser

- (instancetype)initWithData:(NSDictionary *)userData {
    
    self = [super init];
    
    if (self) {
        
        self.firstname = userData[@"FirstName"];
        self.lastname = userData[@"LastName"];
        self.number = userData[@"Email"];
        self.pushIDs = userData[@"MobilePushIds"];
        self.lastSeenDate = userData[@"LastSeenDate"];
    }
    
    return self;
}

#pragma mark - NSCoding

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super init];
    
    if (self) {
        
        self.firstname = [aDecoder decodeObjectForKey:@"firstname"];
        self.lastname = [aDecoder decodeObjectForKey:@"lastname"];
        self.number = [aDecoder decodeObjectForKey:@"email"];
        self.pushIDs = [aDecoder decodeObjectForKey:@"pushIDs"];
        self.lastSeenDate = [aDecoder decodeObjectForKey:@"lastSeenDate"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:self.firstname forKey:@"firstname"];
    [aCoder encodeObject:self.lastname forKey:@"lastname"];
    [aCoder encodeObject:self.number forKey:@"email"];
    [aCoder encodeObject:self.pushIDs forKey:@"pushIDs"];
    [aCoder encodeObject:self.lastSeenDate forKey:@"lastSeenDate"];
}


@end
