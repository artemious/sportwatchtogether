//
//  ContactsManager.m
//  Watch Together
//
//  Created by Vladyslav Filipov on 10/23/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import <Contacts/Contacts.h>
#import "ContactsManager.h"
#import "Contact.h"
#import "Utils.h"
#import "UserManager.h"
#import "ApiManager.h"

@implementation ContactsManager

+ (void)getContacts:(EndBlock)completion {
    
    CNContactStore *store = [[CNContactStore alloc] init];
    [store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted == YES) {
            
            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey, CNContactImageDataKey];
            NSString *containerId = store.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            NSMutableArray* contactsArray;
            
            if (error) {
                NSLog(@"error fetching contacts %@", error);
            } else {
                NSString *phone;
                NSString *fullName;
                NSString *firstName;
                NSString *lastName;
                NSMutableArray *contactNumbersArray;
                contactsArray = [NSMutableArray new];
                
                for (CNContact *contact in cnContacts) {
                    firstName = contact.givenName;
                    lastName = contact.familyName;
                    if (lastName == nil) {
                        fullName=[NSString stringWithFormat:@"%@",firstName];
                    }else if (firstName == nil){
                        fullName=[NSString stringWithFormat:@"%@",lastName];
                    }
                    
                    else{
                        fullName=[NSString stringWithFormat:@"%@ %@",firstName,lastName];
                    }
                    
                    for (CNLabeledValue *label in contact.phoneNumbers) {
                        phone = [label.value stringValue];
                        if ([phone length] > 0) {
                            [contactNumbersArray addObject:phone];
                        }
                    }
                    
                    NSString *newPhoneString = [[phone componentsSeparatedByCharactersInSet:
                                                 [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                                componentsJoinedByString:@""];
                    
                    if ([Utils validateUkNumberString:newPhoneString]) {
                        
                        Contact* currentContact = [[Contact alloc] initWithNumber:[Utils sha256HashFor:[Utils validateUkNumberString:newPhoneString]] number:[Utils validateUkNumberString:newPhoneString] image:contact.imageData pushID:nil andName:fullName];
                        
                        [contactsArray addObject:currentContact];
                    }
                }
                
                NSArray * sortedContacts = [contactsArray sortedArrayUsingComparator:^NSComparisonResult(id firstContact, id secondContact) {
                    NSString *firstContactsName = [(Contact*)firstContact name];
                    NSString *secondContactsName = [(Contact*)secondContact name];
                    return [firstContactsName compare:secondContactsName];
                }];
                
                [UserManager shared].currentUser.userPhoneContacts = [NSMutableArray arrayWithArray:sortedContacts];
                [[UserManager shared].currentUser save];
                
                completion();
            }
        }
    }];
}

+(void)getFriends:(UIViewController*)onController and:(EndBlock)completion {
    
    [UserManager shared].currentUser.userSereverContacts = [NSMutableArray new];
    
    [[ApiManager shared] getRoomUsers:^(id responce, NSError *error) {
        
        if ([responce[@"ok"]  intValue] == 0 && [responce[@"code"]  intValue] == 2) {
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentUser"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            
            if ([responce isKindOfClass:[NSDictionary class]] && [responce[@"data"] isKindOfClass:[NSDictionary class]] && [responce[@"data"][@"friends"] isKindOfClass:[NSArray class]]) {
                
                for (NSDictionary *viewerData in (NSArray*)responce[@"data"][@"friends"]) {
                    
                    for (Contact* phoneBookContact in [UserManager shared].currentUser.userPhoneContacts) {
                        
                        if ([phoneBookContact.numberHash isEqualToString:viewerData[@"phone_hash"]]) {
                            
                            if (viewerData[@"push_id"]) {
                                
                                phoneBookContact.pushId = viewerData[@"push_id"];
                                phoneBookContact.deviceIdHash = viewerData[@"device_id_hash"];
                                
                                [[UserManager shared].currentUser.userSereverContacts addObject:phoneBookContact];
                            }
                        }
                    }
                }
            } else {
                
                [Utils showAlertWithError:error fromViewController:onController];
            }
        }
        
        completion();
    }];
}

@end
