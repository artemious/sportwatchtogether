//
//  ContactsManager.h
//  BTSport
//
//  Created by Vladyslav Filipov on 10/23/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^EndBlock)(void);

@protocol SendingMessages <NSObject>

-(void)sendMessage:(UIViewController*)messageController;

@end

@interface ContactsManager : NSObject
+ (void)getContacts:(EndBlock)completion;
+ (void)getFriends:(UIViewController*)onController and:(EndBlock)completion ;
@end
