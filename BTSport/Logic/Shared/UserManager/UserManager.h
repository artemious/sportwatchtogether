//
//  UserManager.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import <Foundation/Foundation.h>

#import "User.h"

@interface UserManager : NSObject

@property (nonatomic, strong) User *currentUser;

+ (instancetype)shared;

@end
