//
//  StreamChannel.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface StreamChannel : NSObject

@property (nonatomic, strong) NSString* streamURL;
@property (nonatomic, strong) NSString* streamTitle;
@property (nonatomic, strong) NSString* streamStartTime;
@property (nonatomic, strong) NSString* streamEndTime;
@property (nonatomic, strong) NSString* streamLinkId;
@property (nonatomic, strong) NSString* streamImageURLString;

- (id)initWithData:(NSDictionary*)streamData;

@end
