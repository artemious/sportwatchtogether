//
//  StreamChannel.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "StreamChannel.h"

@implementation StreamChannel

- (id)initWithData:(NSDictionary*)streamData {
    
    StreamChannel *stream = [[StreamChannel alloc] init];
    
    stream.streamURL = streamData[@"url"];
    stream.streamTitle = streamData[@"title"];
    stream.streamStartTime = streamData[@"start_time"];
    stream.streamEndTime = streamData[@"end_time"];
//    stream.streamLinkId = streamData[@"LinkId"];
    
    //stream.streamImageURLString = streamData[@"thumbnails"][@"medium"][@"url"];

    
    return stream;
}

@end
