//
//  User.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface User : NSObject <NSCoding>

@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *numberhash;
@property (nonatomic, strong) NSString *devicename;
@property (nonatomic, strong) NSString *deviceid;
@property (nonatomic, strong) NSString *firstname;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic, strong) NSString *fullname;
@property (nonatomic, strong) NSString *roomID;
@property (nonatomic, strong) NSString *roomToConnectTo;
@property (nonatomic, strong) NSString *currentRoom;
@property (nonatomic, strong) NSString *pushID;
@property (nonatomic, strong) NSString *userVideoURL;
@property (nonatomic, strong) NSString *userVideoName;
@property (nonatomic, strong) NSString *appVersion;
@property (nonatomic, strong) NSString *token;
@property (nonatomic) BOOL firstLogin;
@property (nonatomic) BOOL inviteEnabled;

@property (nonatomic, strong) NSMutableArray *userPhoneContacts;
@property (nonatomic, strong) NSMutableArray *userSereverContacts;
@property (nonatomic) NSMutableArray *userPeers;


- (void)addItemsObserver:(id)object;
- (void)removeItemsObserver:(id)object;

+ (instancetype) initWithData:(NSString*)number hashNumber:(NSString*)hashNumber deviceName:(NSString*)deviceName firstName:(NSString*)firstName lastName:(NSString*)lastName deviceId:(NSString*)deviceId pushId:(NSString*)pushId;

+ (instancetype)getSavedUser;

- (void)save;

@end
