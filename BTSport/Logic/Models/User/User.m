//
//  User.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//

#import "User.h"

@implementation User

#pragma mark - Public

- (void)save {
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"currentUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - NSCoding

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super init];
    
    if (self) {
        
        self.userPeers = [NSMutableArray new];
        self.number = [aDecoder decodeObjectForKey:@"number"];
        self.token = [aDecoder decodeObjectForKey:@"token"];
        self.firstname = [aDecoder decodeObjectForKey:@"firstname"];
        self.numberhash =  [aDecoder decodeObjectForKey:@"numberHash"];
        self.deviceid = [aDecoder decodeObjectForKey:@"deviceId"];
        self.devicename = [aDecoder decodeObjectForKey:@"deviceName"];
        self.fullname = [aDecoder decodeObjectForKey:@"fullname"];
        self.roomID = [aDecoder decodeObjectForKey:@"roomID"];
        self.roomToConnectTo = [aDecoder decodeObjectForKey:@"roomToConnectTo"];
        self.currentRoom = [aDecoder decodeObjectForKey:@"currentRoom"];
        self.pushID = [aDecoder decodeObjectForKey:@"pushID"];
        self.firstLogin = [aDecoder decodeBoolForKey:@"isFirstLogin"];
        self.inviteEnabled = [aDecoder decodeBoolForKey:@"isInviteEnabled"];
        self.appVersion =  [aDecoder decodeObjectForKey:@"appVersion"];
    }
    
    return self;
}

-(NSMutableArray*) userPeers {
    
    
    return [self mutableArrayValueForKey:@"_userPeers"];
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:self.number forKey:@"number"];
    [aCoder encodeObject:self.token forKey:@"token"];
    [aCoder encodeObject:self.numberhash forKey:@"numberHash"];
    [aCoder encodeObject:self.firstname forKey:@"firstname"];
    [aCoder encodeObject:self.deviceid forKey:@"deviceId"];
    [aCoder encodeObject:self.devicename forKey:@"deviceName"];
    [aCoder encodeObject:self.fullname forKey:@"fullname"];
    [aCoder encodeObject:self.roomID forKey:@"roomID"];
    [aCoder encodeObject:self.roomToConnectTo forKey:@"roomToConnectTo"];
    [aCoder encodeObject:self.currentRoom forKey:@"currentRoom"];
    [aCoder encodeObject:self.pushID forKey:@"pushID"];
    [aCoder encodeBool:self.firstLogin forKey:@"isFirstLogin"];
    [aCoder encodeBool:self.inviteEnabled forKey:@"isInviteEnabled"];
    [aCoder encodeObject:self.appVersion forKey:@"appVersion"];
}

#pragma mark - Class Methods

+ (instancetype) initWithData:(NSString*)number hashNumber:(NSString*)hashNumber deviceName:(NSString*)deviceName firstName:(NSString*)firstName lastName:(NSString*)lastName deviceId:(NSString*)deviceId pushId:(NSString*)pushId {
    
    User *user = [[User alloc] init];
    
    user.userPeers = [NSMutableArray new];

    user.number = number;
    user.numberhash = hashNumber;
    user.devicename = deviceName;
    user.deviceid = deviceId;
    user.firstname = firstName;
    user.lastname = lastName;
    user.fullname = [NSString stringWithFormat:@"%@ %@", user.firstname, user.lastname];
    user.pushID = pushId;
    user.roomID = user.deviceid;
    
    user.roomToConnectTo = user.roomID;
    user.currentRoom = nil;
    
    return user;
}

- (void)addItemsObserver:(id)object {

    [self addObserver:object forKeyPath:@"_userPeers.@count" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:nil];
}

- (void)removeItemsObserver:(id)object {
    

    [self removeObserver:object forKeyPath:@"_userPeers.@count"];
}

+ (instancetype)getSavedUser {
    
    NSData *userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"];
    
    User *currentUser = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
    
    return currentUser;
}

@end
