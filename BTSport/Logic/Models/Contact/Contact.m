//
//  Contact.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/24/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import "Contact.h"

@implementation Contact

-(instancetype)initWithNumber:(NSString*)numberHash number:(NSString*)number image:(NSData*) imageData pushID:(NSString*)pushID andName:(NSString*)name {
    
    self.name = name;
    
    if (imageData) {
        
        self.image = imageData;
    }
    
    if(number) {
        
        self.number = number;
    }
    
    self.numberHash = numberHash;
    
    if (pushID) {
        
        self.pushId = pushID;
    }
    
    return self;
    
}


- (void)encodeWithCoder:(nonnull NSCoder *)aCoder {
  
    [aCoder encodeObject:self.number forKey:@"number"];
    [aCoder encodeObject:self.numberHash forKey:@"numberHash"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.pushId forKey:@"pushId"];
    [aCoder encodeObject:self.image forKey:@"image"];
}

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder {
    
    self = [super init];
    
    if (self) {
        
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.number = [aDecoder decodeObjectForKey:@"number"];
        self.numberHash = [aDecoder decodeObjectForKey:@"numberHash"];
        self.pushId = [aDecoder decodeObjectForKey:@"pushId"];
        self.image = [aDecoder decodeObjectForKey:@"image"];
    }
    
    return self;
    
}

- (BOOL)isEqual:(_Nullable id) other {
    
    if ([other isKindOfClass:[self class]]) {
        Contact * contact = (Contact*) other;
        
        return [self.numberHash isEqualToString:contact.numberHash ];
    }
    
    return NO;
}

- (NSUInteger)hash {
    return self.numberHash.hash ^ self.number.hash;
}

@end
