//
//  Contact.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/24/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject <NSCoding>

@property (nonatomic, strong) NSString *number;
@property (nonatomic, strong) NSString *numberHash;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *pushId;
@property (nonatomic, strong) NSString *deviceIdHash;
@property (nonatomic, strong) NSData *image;


-(instancetype)initWithNumber:(NSString*)numberHash number:(NSString*)number image:(NSData*) imageData pushID:(NSString*)pushID andName:(NSString*)name;

@end


