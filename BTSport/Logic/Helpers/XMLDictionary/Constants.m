//
//  Constants.m
//  BTSport
//
//  Created by Bunyk Jaroslav on 3/19/18.
//  Copyright © 2018 JaroslavBunyk. All rights reserved.
//

#import "Constants.h"

NSString* const SplashToLoginSegue = @"SPLASH_TO_LOGIN_SEGUE";
NSString* const SplashToSearchSegue = @"SPLASH_TO_SEARCH_SEGUE";
NSString* const SplashToMainSegue = @"SPLASH_TO_MAIN_SEGUE";
NSString* const LoginToSearchSegue = @"LOGIN_TO_SEARCH_SEGUE";
NSString* const LoginToMainSegue = @"LOGIN_TO_MAIN_SEGUE";
NSString* const SearchToMainSegue = @"SEARCH_TO_MAIN_SEGUE";
NSString* const ChanelListToStreamList = @"CHANEL_LIST_TO_STREAM_LIST";
NSString* const StreamListToStreamVideo = @"STREAM_LIST_TO_STREAM";
NSString* const StreamToHistory = @"STREAM_TO_HISTORY";
NSString* const ChanelListToStreamVideo = @"CHANNEL_TO_STREAM";
