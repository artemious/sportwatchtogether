//
//  Constants.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "Constants.h"

NSString* const SplashToLoginSegue = @"SPLASH_TO_LOGIN_SEGUE";
NSString* const SplashToStreamList = @"SPLASH_TO_STREAM_LIST";
NSString* const LoginFriendsist = @"LOGIN_TO_FRIENDS_LIST";
NSString* const LoginToStreamList = @"LOGIN_TO_STREAM_LIST";
NSString* const StreamsListToStreamDetails = @"STREAMS_LIST_TO_STREAM_DETAILS";
NSString* const StreamsListToStreamWatch = @"STREAMS_LIST_TO_STREAM_WATCH";
NSString* const StreamDetailsToStreamWatch = @"STREAM_DETAILS_TO_STREAM_WATCH";
NSString* const StreamWatchToHistory = @"STREAM_WATCH_TO_HISTORY";
NSString* const GetContactsToCantactsScreen = @"showContacts";
NSString* const ContactsToMainScreen = @"contacts_to_main";
NSString* const FromLoginToMain = @"fromLoginToMain";


