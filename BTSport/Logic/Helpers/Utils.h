//
//  Utils.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Branch.h"
#import <UIKit/UIKit.h>


@interface Utils : NSObject

+(NSString*)validateUkNumberString:(NSString*)contactNumber;

+(NSString*)connectedNetworkType;

+(NSString*)sha256HashFor:(NSString*)input;

+ (void)showAlertWithError:(NSError*)error fromViewController:(UIViewController*)controller;

+ (BranchUniversalObject*)getBranchUniversalObjectWith:(NSString*)videoStreamID and:(NSString*)channelName;

+ (BOOL)isJailBroken;
@end
