//
//  Utils.m
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import "Utils.h"

#include <sys/stat.h>

#import <CoreTelephony/CTTelephonyNetworkInfo.h>

#import "UserManager.h"

#import <CommonCrypto/CommonDigest.h>

#import "Reachability.h"

@implementation Utils

+(NSString*)connectedNetworkType {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];

    NetworkStatus status = [reachability currentReachabilityStatus];

    if(status == NotReachable) {
        NSLog(@"none");
        //No internet
    }
    else if (status == ReachableViaWiFi) {
        NSLog(@"Wifi");
        //WiFi
        return @"Wifi";
    }
    else if (status == ReachableViaWWAN){
        NSLog(@"WWAN");
        //connection type
        CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
        //    _carrier = [[netinfo subscriberCellularProvider] carrierName];
        if (([netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyGPRS])
            ||([netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyEdge])
            ||([netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyCDMA1x])) {
            NSLog(@"2G");
            return @"2G";
        }
        else if (([netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyWCDMA])
                 ||([netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyHSDPA])
                 ||([netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyHSUPA])
                 ||([netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyCDMAEVDORev0])
                 ||([netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyCDMAEVDORevA])
                 ||([netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyCDMAEVDORevB])
                 ||([netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyeHRPD])){
            NSLog(@"3G");
            return @"3G";
        }
        else if ([netinfo.currentRadioAccessTechnology isEqualToString:CTRadioAccessTechnologyLTE]) {
            NSLog(@"4G");
            return @"4G";

        }
    }
    return @"-1";//default unknown
}

+(NSString*)validateUkNumberString:(NSString*)contactNumber {
    
    NSString* number = nil;
    
    if (contactNumber.length > 5) {
        
        if ([[contactNumber substringWithRange:NSMakeRange(0, 2)] isEqualToString:@"07"] ) {
            
            NSString* currentContact = [contactNumber stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"7"];
            
            number =  [NSString stringWithFormat:@"+44%@", currentContact];
            
        }
        
        if ([[contactNumber substringWithRange:NSMakeRange(0, 3)] isEqualToString:@"447"]){
            
            number = [NSString stringWithFormat:@"+%@", contactNumber];
        }
        
        if ([[contactNumber substringWithRange:NSMakeRange(0, 3)] isEqualToString:@"+44"]){
            
            number = contactNumber;
        }
    }
    
    return number;
}

+(NSString*)sha256HashFor:(NSString*)input {
    
    const char* str = [input UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++) {
        
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}


+ (void)showAlertWithError:(NSError*)error fromViewController:(UIViewController*)controller {
    
    NSString* errorMessage = error.localizedDescription;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction
                      actionWithTitle:@"Ok"
                      style:UIAlertActionStyleDefault
                      handler:nil]];
    
    [controller presentViewController:alert animated:YES completion:nil];
    
}

+ (BranchUniversalObject*)getBranchUniversalObjectWith:(NSString*)videoStreamID and:(NSString*)channelName {
    
    BranchUniversalObject *branchUniversalObject = [[BranchUniversalObject alloc] initWithCanonicalIdentifier:@"watchtogether"];
    branchUniversalObject.title = @"Hey,"; 
    branchUniversalObject.contentDescription = [NSString stringWithFormat:@"download the BT SPORT Watch Together app so we can watch sports together:%@", [UserManager shared].currentUser.fullname];
    
    branchUniversalObject.contentMetadata.customMetadata[@"room"] = [UserManager shared].currentUser.currentRoom ? [UserManager shared].currentUser.currentRoom : [UserManager shared].currentUser.roomID;
    branchUniversalObject.contentMetadata.customMetadata[@"channel_id"] = @"Test";
    branchUniversalObject.contentMetadata.customMetadata[@"channel_name"] = channelName;
    branchUniversalObject.contentMetadata.customMetadata[@"show_name"] = @"Test";
    branchUniversalObject.contentMetadata.customMetadata[@"video_id"] = videoStreamID;
    branchUniversalObject.contentMetadata.customMetadata[@"sender_name"] = [UserManager shared].currentUser.firstname;
    branchUniversalObject.contentMetadata.customMetadata[@"date_mls"] = [NSString stringWithFormat: @"%f", [[NSDate date]timeIntervalSince1970]];

    return branchUniversalObject;
}

+ (BOOL)isJailBroken {

    BOOL pathes = NO;
    BOOL tryToWrite = NO;

    NSString* bundlePath = [[NSBundle mainBundle] bundlePath];
    
    // scan for itunes metadata
    BOOL isDirectory = NO;
    NSString* directoryPath = [bundlePath stringByAppendingPathComponent:@"SC_Info/"];
    BOOL directoryIsAvailable = [[NSFileManager defaultManager] fileExistsAtPath:directoryPath isDirectory:&isDirectory];
    BOOL contentSeemsValid = ([[[NSFileManager defaultManager] contentsOfDirectoryAtPath:directoryPath error:NULL] count] == 2);
   
    if (directoryIsAvailable && contentSeemsValid) {
        
        return YES;
    }
    
    
    contentSeemsValid = [[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/iTunesMetadata.​plist", bundlePath]];
    if (contentSeemsValid) {
        
        return YES;
    }
    
    NSArray *paths =     @[ @"/private/var/stash",
                            @"/private/var/lib/apt",
                            @"/private/var/tmp/cydia.log",
                            @"/private/var/lib/cydia",
                            @"/private/var/mobile/Library/SBSettings/Themes",
                            @"/Library/MobileSubstrate/MobileSubstrate.dylib",
                            @"/Library/MobileSubstrate/DynamicLibraries/Veency.plist",
                            @"/Library/MobileSubstrate/DynamicLibraries/LiveClock.plist",
                            @"/System/Library/LaunchDaemons/com.ikey.bbot.plist",
                            @"/System/Library/LaunchDaemons/com.saurik.Cydia.Startup.plist",
                            @"/var/cache/apt",
                            @"/var/lib/apt",
                            @"/var/lib/cydia",
                            @"/var/log/syslog",
                            @"/var/tmp/cydia.log",
                            @"/bin/bash",
                            @"/bin/sh",
                            @"/usr/sbin/sshd",
                            @"/usr/libexec/ssh-keysign",
                            @"/usr/sbin/sshd",
                            @"/usr/bin/sshd",
                            @"/usr/libexec/sftp-server",
                            @"/etc/ssh/sshd_config",
                            @"/etc/apt",
                            @"/Applications/Cydia.app",
                            @"/Applications/RockApp.app",
                            @"/Applications/Icy.app",
                            @"/Applications/WinterBoard.app",
                            @"/Applications/SBSettings.app",
                            @"/Applications/MxTube.app",
                            @"/Applications/IntelliScreen.app",
                            @"/Applications/FakeCarrier.app",
                            @"/Applications/blackra1n.app",
                            ];
    
    
    for (NSString *path in paths) {
        
        if ([self fileExistsAtPath:path]) {
            
            //Device is jailbroken
            
            pathes =  YES;
        }
    }
    
    //Symbolic link verification
    
    struct stat s;
    
    if(lstat("/Applications", &s) || lstat("/var/stash/Library/Ringtones", &s) || lstat("/var/stash/Library/Wallpaper", &s)
       || lstat("/var/stash/usr/include", &s) || lstat("/var/stash/usr/libexec", &s)  || lstat("/var/stash/usr/share", &s) || lstat("/var/stash/usr/arm-apple-darwin9", &s))
    {
        if(s.st_mode & S_IFLNK){
            return YES;
        }
    }
    
    // scan for forking
    int forkValue = fork();
    if (forkValue >= 0) {
        return YES;
    }
    
    NSError *error;
    NSString *stringToBeWritten = @"This is a test.";
    [stringToBeWritten writeToFile:@"/private/jailbreak.txt" atomically:YES
                          encoding:NSUTF8StringEncoding error:&error];
    if(error==nil){
        //Device is jailbroken
        tryToWrite =  YES;
    } else {
        //Device is not jailbroken
        [[NSFileManager defaultManager] removeItemAtPath:@"/private/jailbreak.txt" error:nil];
        
        tryToWrite =  NO;
    }
   
//         Check if the app can open a Cydia's URL scheme
        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"cydia://"]]) {
            return YES;
        }
    
    if (pathes == NO && tryToWrite == NO) {
        
        return NO;
    }else {
        
        return YES;
    }
}

+ (BOOL)fileExistsAtPath:(NSString *)path {
    
    FILE *pFile;
    pFile = fopen([path cStringUsingEncoding:[NSString defaultCStringEncoding]], "r");
    if (pFile == NULL) {
        return NO;
    }
    else
        fclose(pFile);
    return YES;
}

@end
