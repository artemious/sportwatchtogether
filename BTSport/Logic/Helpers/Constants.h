//
//  Constants.h
//  Watch Together
//
//  Created by Артём Гуральник on 9/19/18.
//  Copyright © 2018 SmartExe. All rights reserved.
//


#import <Foundation/Foundation.h>

#pragma mark - Segues

extern NSString* const SplashToLoginSegue;
extern NSString* const SplashToStreamList;
extern NSString* const LoginToStreamList;
extern NSString* const StreamsListToStreamDetails;
extern NSString* const StreamsListToStreamWatch;
extern NSString* const StreamDetailsToStreamWatch;
extern NSString* const StreamWatchToHistory;
extern NSString* const GetContactsToCantactsScreen;
extern NSString* const LoginFriendsist;
extern NSString* const ContactsToMainScreen;
extern NSString* const FromLoginToMain;

typedef enum  ContactsViewType {
    
    kServerContactsType,
    kPhoneContactsType
}ContactsViewType;
